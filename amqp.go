package gobase

import (
	"encoding/json"
	"errors"
	"github.com/streadway/amqp"
	"log"
	"strconv"
	"time"
)

// IAMQMessage interface
type IAMQMessage interface {
	IsPersistent() bool
	GetContentType() string
	GetExpiresIn() int
	GetBody() ([]byte, error)
	GetData() *AMQMessageData
}

// AMQMessageData struct
type AMQMessageData struct {
	Text string `json:"text"`
}

// AMQMessage Global struct
type AMQMessage struct {
	ContentType string          `json:"content_type"`
	Persistent  bool            `json:"persistent"`
	ExpiresIn   int             `json:"expires_in"`
	Data        *AMQMessageData `json:"data"`
}

// Enforce Interface implementation
var _ IAMQMessage = (*AMQMessage)(nil)

// GetContentType message content type, always defaulting to application/json
func (c *AMQMessage) GetContentType() string {
	if c.ContentType == "" {
		c.ContentType = "application/json"
	}
	return c.ContentType
}

// IsPersistent message persistency
func (c *AMQMessage) IsPersistent() bool {
	return c.Persistent
}

// GetData message data
func (c *AMQMessage) GetData() *AMQMessageData {
	return c.Data
}

// GetExpiresIn message data
func (c *AMQMessage) GetExpiresIn() int {
	return c.ExpiresIn
}

// GetBody message content type
func (c *AMQMessage) GetBody() ([]byte, error) {

	// Shortcut to content type
	contentType := c.GetContentType()

	// Lookup content type
	switch contentType {

	// JSON type, just delegate to marshalling
	case "application/json":
		return json.Marshal(c.Data)

	// Non-supported content type
	default:
		return nil, errors.New("Unsupported content type " + contentType)
	}
}

// IAMQConnection interface
type IAMQConnection interface {
	Close() error
	Channel() (IAMQChannel, error)
}

// RabbitMQConnection Global struct
type RabbitMQConnection struct {
	Conn *amqp.Connection
}

// Enforce Interface implementation
var _ IAMQConnection = (*RabbitMQConnection)(nil)

// Close connection
func (c *RabbitMQConnection) Close() error {
	return c.Conn.Close()
}

// Channel declaration
func (c *RabbitMQConnection) Channel() (IAMQChannel, error) {
	ch, err := c.Conn.Channel()
	if err != nil {
		return nil, err
	}
	return &RabbitMQChannel{Ch: ch}, nil
}

// IAMQChannel interface
type IAMQChannel interface {
	GetChannel() interface{}
	Publish(string, string, bool, bool, IAMQMessage) error
	ExchangeDeclare(string, string, bool, bool, bool, bool, map[string]interface{}) error
	QueueDeclare(string, bool, bool, bool, bool, map[string]interface{}) (interface{}, error)
	QueueBind(string, string, string, bool, map[string]interface{}) error
	Consume(string, string, bool, bool, bool, bool, map[string]interface{}) (interface{}, error)
}

// RabbitMQChannel Global struct
type RabbitMQChannel struct {
	Ch            *amqp.Channel
	QueueChannels map[string]<-chan amqp.Delivery
}

// Enforce Interface implementation
var _ IAMQChannel = (*RabbitMQChannel)(nil)

// GetChannel Gets underlying channel
func (c *RabbitMQChannel) GetChannel() interface{} {
	return c.Ch
}

// Publish message into channel
func (c *RabbitMQChannel) Publish(exchange string, key string, mandatory bool, immediate bool, msg IAMQMessage) error {

	// First of all, get message proper bytecode body
	bBody, err := msg.GetBody()
	if err != nil {
		log.Printf("Error trying to get bytecode of a mesage body: %v", err)
		return err
	}

	// Get delivery mode
	var deliveryMode uint8
	if msg.IsPersistent() {
		deliveryMode = amqp.Persistent
	}

	// Get content type
	contentType := msg.GetContentType()
	if contentType == "" {
		contentType = "application/json"
	}

	expiration := ""
	expiresIn := msg.GetExpiresIn()
	if expiresIn > 0 {
		expiration = strconv.Itoa(expiresIn)
	}

	// Build RabbitMQ publish message from our abstracted one
	message := amqp.Publishing{
		Timestamp:    time.Now(),
		DeliveryMode: deliveryMode,
		ContentType:  contentType,
		Expiration:   expiration,
		Body:         bBody,
	}

	// Delegate into RabbitMQ publish method
	return c.Ch.Publish(exchange, key, mandatory, immediate, message)
}

// ExchangeDeclare declares an exchange into a channel
func (c *RabbitMQChannel) ExchangeDeclare(name string, exchangeType string, persistent bool, autoDelete bool, internal bool, noWait bool, args map[string]interface{}) error {
	return c.Ch.ExchangeDeclare(name, exchangeType, persistent, autoDelete, internal, noWait, args)
}

// QueueDeclare declares a queue into a channel
func (c *RabbitMQChannel) QueueDeclare(name string, durable bool, autoDelete bool, exclusive bool, noWait bool, args map[string]interface{}) (interface{}, error) {
	return c.Ch.QueueDeclare(name, durable, autoDelete, exclusive, noWait, args)
}

// QueueBind binds a queue into a channel
func (c *RabbitMQChannel) QueueBind(name string, topic string, exchangeName string, noWait bool, args map[string]interface{}) error {
	return c.Ch.QueueBind(name, topic, exchangeName, noWait, args)
}

// Consume consumes a queue
func (c *RabbitMQChannel) Consume(queue string, consumer string, autoAck bool, exclusive bool, noLocal bool, noWait bool, args map[string]interface{}) (interface{}, error) {
	return c.Ch.Consume(queue, consumer, autoAck, exclusive, noLocal, noWait, args)
}

// IAMQClientRole interface
type IAMQClientRole interface {
	GetClient() IAMQClient
	GetConnection() IAMQConnection
	GetChannel() IAMQChannel
}

// IAMQClient interface
type IAMQClient interface {
	Init() (IAMQConsumer, IAMQProducer, error)
	Connect() (IAMQConnection, IAMQChannel, error)
	GetConfig() interface{}
}

// AMQQueueConfigOptions RabbitMQ queues config options model
type AMQQueueConfigOptions struct {
	Exclusive bool                   `json:"exclusive"`
	Durable   bool                   `json:"durable"`
	Arguments map[string]interface{} `json:"arguments"`
}

// AMQQueueConfig RabbitMQ queues config model
type AMQQueueConfig struct {
	Topic     string                `json:"topic"`
	Options   AMQQueueConfigOptions `json:"options"`
	Temporary bool                  `json:"temporary"`
	Ack       bool                  `json:"ack"`
}

// RabbitMQConfig RabbitMQ config model
type RabbitMQConfig struct {
	Hosts           []string                  `json:"hosts"`
	User            string                    `json:"user"`
	Password        string                    `json:"password"`
	DefaultExchange string                    `json:"default_exchange"`
	QueuesConfig    map[string]AMQQueueConfig `json:"queues_config"`
	ConsumerQueues  []string                  `json:"consumer_queues"`
}

// RabbitMQClient Global struct
type RabbitMQClient struct {
	Config RabbitMQConfig
}

// Enforce Interface implementation
var _ IAMQClient = (*RabbitMQClient)(nil)

// fixQueueOptionsTypes just fixes some annoying casts from interfaces
func (c *RabbitMQClient) fixQueueOptionsTypes() {
	for _, qc := range c.Config.QueuesConfig {
		for k, v := range qc.Options.Arguments {
			switch aType := v.(type) {
			default:
				log.Printf("unexpected type %T for %s (%v)", aType, k, v)
			case int:
				qc.Options.Arguments[k] = int64(v.(int))
			case nil, bool, byte, int16, int32, int64, float32, float64, string, []byte, time.Time:
				// log.Printf("TYPE: %T", aType)
			}
			switch k {
			case "x-message-ttl":
				val, ok := v.(int)
				if ok {
					qc.Options.Arguments[k] = int64(val)
					continue
				}
				val2, ok := v.(float64)
				if ok {
					qc.Options.Arguments[k] = int64(val2)
					continue
				}
				val3, ok := v.(float32)
				if ok {
					qc.Options.Arguments[k] = int64(val3)
					continue
				}
			}
		}
	}
}

// Init Client
func (c *RabbitMQClient) Init() (IAMQConsumer, IAMQProducer, error) {

	// Fix queue arguments first
	c.fixQueueOptionsTypes()

	// Get a connection + channel and bootstrap a consumer
	cConn, cChannel, err := c.Connect()
	if err != nil {
		return nil, nil, err
	}
	consumer := RabbitMQConsumer{Client: c, Connection: cConn, Channel: cChannel}

	// Loop over declared consumer queues
	for _, cq := range c.Config.ConsumerQueues {

		// Get from configuration or fail
		qc, ok := c.Config.QueuesConfig[cq]
		if !ok {
			return nil, nil, errors.New("Queue consumer defined but not declared: " + cq)
		}

		// Start consumer or fail
		err := consumer.ConsumeQueue(cq, qc, c.Config.DefaultExchange)
		if err != nil {
			return nil, nil, err
		}
	}

	// Get a connection + channel and bootstrap a producer
	pConn, pChannel, err := c.Connect()
	if err != nil {
		return nil, nil, err
	}
	producer := RabbitMQProducer{Client: c, Connection: pConn, Channel: pChannel}

	// Return both
	return &consumer, &producer, nil
}

// Connect Client and return connection (or error)
func (c *RabbitMQClient) Connect() (IAMQConnection, IAMQChannel, error) {

	// Connect thru dialing to host(s), store it and defer its close
	// TODO: Deal with cluster here, aka just randomize and pick up one from Hosts array
	connectURL := "amqp://" + c.Config.User + ":" + c.Config.Password + "@" + c.Config.Hosts[0] + "/"
	conn, err := amqp.Dial(connectURL)
	if err != nil {
		log.Printf("connection.open: %s", err)
		return nil, nil, err
	}
	connection := RabbitMQConnection{Conn: conn}

	// Declare channel or fail
	ch, err := connection.Channel()
	if err != nil {
		log.Printf("channel.open: %s", err)
		return nil, nil, err
	}

	// Set exchange type to default one, chances to move to another one are practically 0
	exchangeType := RabbitMqDefaultExchangeType

	// Get the default exchange, chances to change it are really low too
	exchangeName := c.Config.DefaultExchange

	// Declare exchange
	err = ch.ExchangeDeclare(exchangeName, exchangeType, true, false, false, false, nil)
	if err != nil {
		log.Printf("exchange.declare: %v", err)
		return nil, nil, err
	}

	// Return connection and channel
	return &connection, ch, nil
}

// GetConfig gets the underlying config
func (c *RabbitMQClient) GetConfig() interface{} {
	return c.Config
}

// RabbitMqDefaultExchangeType Default exchange type
var RabbitMqDefaultExchangeType = "topic"

// IAMQProducer interface
type IAMQProducer interface {
	IAMQClientRole
	Publish(string, string, IAMQMessage) error
}

// RabbitMQProducer Global struct
type RabbitMQProducer struct {
	Client     IAMQClient
	Connection IAMQConnection
	Channel    IAMQChannel
}

// Enforce Interface implementation
var _ IAMQProducer = (*RabbitMQProducer)(nil)

// GetClient gets the underlying client
func (c *RabbitMQProducer) GetClient() IAMQClient {
	return c.Client
}

// GetConnection gets the underlying client
func (c *RabbitMQProducer) GetConnection() IAMQConnection {
	return c.Connection
}

// GetChannel gets the underlying client
func (c *RabbitMQProducer) GetChannel() IAMQChannel {
	return c.Channel
}

// Publish an AMQP message
func (c *RabbitMQProducer) Publish(exchangeName string, routingKey string, msg IAMQMessage) error {

	// Info
	log.Printf("[AMQP] Sending to %s : %s message %v", exchangeName, routingKey, msg)

	// Get client config or die
	clientConfig, ok := c.GetClient().GetConfig().(RabbitMQConfig)
	if !ok {
		return errors.New("[AMQP] Could not parse client config")
	}
	if exchangeName == "" {
		exchangeName = clientConfig.DefaultExchange
	}

	// Error if no routing key defined
	if routingKey == "" {
		return errors.New("[AMQP] Should define a routing key to publish a message")
	}

	// Publish message or ... critical log???
	err := c.Channel.Publish(exchangeName, routingKey, true, false, msg)
	if err != nil {
		log.Printf("[AMQP] [PUBLISH] [CRITICAL]: %v", err)
		return err
	}

	// TODO: Add here the Confiration mode with NotifyPublish/NotifyReturn???

	// All good
	return nil
}

// IAMQConsumer interface
type IAMQConsumer interface {
	IAMQClientRole
	ConsumeQueue(string, AMQQueueConfig, string) error
}

// RabbitMQConsumer Global struct
type RabbitMQConsumer struct {
	Client     IAMQClient
	Connection IAMQConnection
	Channel    IAMQChannel
}

// Enforce Interface implementation
var _ IAMQConsumer = (*RabbitMQConsumer)(nil)

// GetClient gets the underlying client
func (c *RabbitMQConsumer) GetClient() IAMQClient {
	return c.Client
}

// GetConnection gets the underlying client
func (c *RabbitMQConsumer) GetConnection() IAMQConnection {
	return c.Connection
}

// GetChannel gets the underlying client
func (c *RabbitMQConsumer) GetChannel() IAMQChannel {
	return c.Channel
}

// ConsumeQueue consumes a queue
func (c *RabbitMQConsumer) ConsumeQueue(queue string, qc AMQQueueConfig, exchangeName string) error {

	// Info
	log.Printf("AMQP Consumer for Queue %s", queue)

	// Set empty name if temporary queue (also empty queue name is allowed)
	qName := queue
	if qc.Temporary {
		qName = ""
	}

	// Fail if no exchange name
	if exchangeName == "" {
		return errors.New("No exchange name provided")
	}

	// Declare queue
	_, err := c.Channel.QueueDeclare(qName, qc.Options.Durable, qc.Temporary, qc.Options.Exclusive, false, qc.Options.Arguments)
	if err != nil {
		log.Printf("queue.declare: %v", err)
		return err
	}

	// Bind queue
	err = c.Channel.QueueBind(qName, qc.Topic, exchangeName, false, nil)
	if err != nil {
		log.Printf("queue.bind: %v", err)
		return err
	}

	// Consume into queue
	qChannel, err := c.Channel.Consume(qName, "", false, qc.Options.Exclusive, false, false, nil)
	if err != nil {
		log.Printf("basic.consume: %v", err)
		return err
	}

	// Cast back channel to proper type or die
	msgChan, ok := qChannel.(<-chan amqp.Delivery)
	if !ok {
		errStr := "Could not cast AMQP Delivery chan"
		log.Println(errStr)
		return errors.New(errStr)
	}

	// Goroutine to handle received messages here
	go func() {

		// Infinite loop
		for msg := range msgChan {

			// Wrap into goroutine, we want parallel processing
			go func(task amqp.Delivery) {

				// TODO: Do the stuff with the task message here
				log.Printf("Received AMQP message: %v", task)

				// Acknowledge if allowed
				if qc.Ack {
					task.Ack(false)
				}
			}(msg)
		}
	}()

	// Return no errors
	return nil
}
