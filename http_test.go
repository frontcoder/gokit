package gobase

import (
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"testing"
	"time"
)

// DummyJSONResource is used for tests
type DummyJSONResource struct {
	HTTPBaseResource
	Name string `json:"name"`
	Rank int    `json:"rank"`
}

// Global variables in the test scope
var httpServerConfig *HTTPServerConfig
var httpServer *HTTPServer
var httpServerProtocol = "http"
var httpServerRootURL string
var httpDummiesRepository1 IHTTPRepository
var httpDummiesRepository2 IHTTPRepository
var httpDummiesRepository3 IHTTPRepository
var currentRepo IHTTPRepository
var dummiesBasePath = "/test/dummies"
var dummiesBaseURL string
var dummyJSONResource1 = DummyJSONResource{
	HTTPBaseResource: HTTPBaseResource{
		ID: "my_id_1",
	},
	Name: "dummy30",
}
var dummyJSONResource2 = DummyJSONResource{
	HTTPBaseResource: HTTPBaseResource{
		ID: "my_id_2",
	},
	Name: "dummy2",
	Rank: 9,
}
var dummyJSONResource3 = DummyJSONResource{
	HTTPBaseResource: HTTPBaseResource{
		ID: "my_id_3",
	},
	Rank: 2,
}
var dummyJSONResourcesMap = map[string]DummyJSONResource{
	dummyJSONResource1.ID.(string): dummyJSONResource1,
	dummyJSONResource2.ID.(string): dummyJSONResource2,
	dummyJSONResource3.ID.(string): dummyJSONResource3,
}
var expectedResponseStatusMap = map[string]string{
	"unauthorized_request":   "401 Unauthorized",
	"resource_not_found":     "404 Not Found",
	"bad_request":            "400 Bad Request",
	"bad_request_no_id":      "No ID provided",
	"bad_request_input_data": "Could not cast query interface to either map or string",
}

// loadCacheEnv reads environment for these tests from globally parsed environment file
func loadHTTPServerEnv() (*HTTPServerConfig, error) {

	// Parse configuration
	config := TestEnv.HTTPServer
	if config == nil {
		return nil, errors.New("Missing HTTP Server environment configuration")
	}

	// Return no error
	return config, nil
}

// TestHTTP is the main test loop
func TestHTTP(t *testing.T) {

	// Load environment configuration or fail
	var err error
	httpServerConfig, err = loadHTTPServerEnv()
	if err != nil {
		t.Error(err.Error())
		return
	}

	// Form root and dummies base URL
	httpServerRootURL = httpServerProtocol + "://127.0.0.1:" + fmt.Sprintf("%d", httpServerConfig.Port)
	dummiesBaseURL = httpServerRootURL + dummiesBasePath

	// Create a test repository
	httpDummiesRepository1 = &HTTPRepository{
		Client: &http.Client{},
		Endpoints: HTTPEndpoints{
			Base: HTTPEndpoint{
				URL: dummiesBaseURL,
				Headers: map[string][]string{
					"Content-Type": []string{"application/json"},
					"test-token":   []string{"sometoken"},
				},
			},
		},
	}

	// Create another test repository
	httpDummiesRepository2 = &HTTPRepository{
		Client: &http.Client{},
		Endpoints: HTTPEndpoints{
			Base: HTTPEndpoint{
				URL: httpServerRootURL,
				Headers: map[string][]string{
					"Content-Type": []string{"application/json"},
				},
			},
			Get: HTTPEndpoint{
				URL: dummiesBasePath,
			},
			Post: HTTPEndpoint{
				URL: dummiesBaseURL,
				Headers: map[string][]string{
					"test-token": []string{"sometoken"},
				},
			},
			Put: HTTPEndpoint{
				URL: dummiesBaseURL,
				Headers: map[string][]string{
					"test-token": []string{"sometoken"},
				},
			},
			Delete: HTTPEndpoint{
				URL: dummiesBaseURL,
				Headers: map[string][]string{
					"test-token": []string{"sometoken"},
				},
			},
		},
	}

	// Create yet another test repository, to fail middleware(s)
	httpDummiesRepository3 = &HTTPRepository{
		Client: &http.Client{},
		Endpoints: HTTPEndpoints{
			Base: HTTPEndpoint{
				URL: dummiesBaseURL,
				Headers: map[string][]string{
					"Content-Type": []string{"application/json"},
				},
			},
		},
	}

	// Create repos slice
	httpDummiesRepos := []IHTTPRepository{httpDummiesRepository1, httpDummiesRepository2}

	// Run tests, adding some delays in between
	if b := t.Run("http server start", testStartHTTP); !b {
		t.Error(errors.New("Could not start http server"))
		return
	}

	// Wait for server to stablilize listening
	time.Sleep(2 * time.Second)

	// TODO: Test regular controllers with HTTP codes, Sessions, etc.

	// Test repositories
	t.Run(fmt.Sprintf("Repo %d - POST without middleware auth token - Should deliver an error", 3), testUnauthRequest)
	var i int
	for i, currentRepo = range httpDummiesRepos {
		t.Run(fmt.Sprintf("Repo %d - FIND with bad input - Should deliver an error", i+1), testFindBadRequest)
		t.Run(fmt.Sprintf("Repo %d - FIND no resources - Should get empty collection", i+1), testFindNoResourcesRequest)
		t.Run(fmt.Sprintf("Repo %d - FIND all resources - Should get all resources", i+1), testFindAllResourcesRequest)
		t.Run(fmt.Sprintf("Repo %d - FIND some sorted resources - Should get some resources sorted", i+1), testFindSortedResourcesRequest)
		t.Run(fmt.Sprintf("Repo %d - GET no ID - Should deliver an error", i+1), testGetBadRequest)
		t.Run(fmt.Sprintf("Repo %d - GET inexistent ID - Should deliver an error", i+1), testGetMissingResource)
		t.Run(fmt.Sprintf("Repo %d - GET existent ID - Should deliver a resource", i+1), testGetExistingResource)
		t.Run(fmt.Sprintf("Repo %d - CREATE bad resource payload - Should deliver an error", i+1), testPostBadResource)
		t.Run(fmt.Sprintf("Repo %d - CREATE valid resource - Should create resource", i+1), testPostValidResource)
		t.Run(fmt.Sprintf("Repo %d - UPDATE inexistent resource - Should deliver an error", i+1), testUpdateBadResource)
		t.Run(fmt.Sprintf("Repo %d - UPDATE created resource - Should update the created resource", i+1), testUpdateCreatedResource)
		t.Run(fmt.Sprintf("Repo %d - DELETE inexistent resource - Should deliver an error", i+1), testDeleteBadResource)
		t.Run(fmt.Sprintf("Repo %d - DELETE created resource - Should delete the created resource", i+1), testDeleteExistingResource)
		t.Run(fmt.Sprintf("Repo %d - FIND deleted resource - Should deliver an error", i+1), testFindBadResourceRequest)
	}
}

// testStartHTTP starts the HTTP server
func testStartHTTP(t *testing.T) {

	// Init HTTP Server object
	httpServer := &HTTPServer{
		Config: *httpServerConfig,
	}

	// Add some middleware
	httpServer.AddMiddleware("test-token", dummyTokenMiddleware)

	// Add controllers/routes
	dummyFindController := dummyFindControllerType{}
	httpServer.AddRoute("GET", dummiesBasePath, dummyFindController.Handle, nil)
	dummyGetController := dummyGetControllerType{}
	httpServer.AddRoute("GET", dummiesBasePath+"/:id", dummyGetController.Handle, nil)
	dummyPostController := dummyPostControllerType{}
	httpServer.AddRoute("POST", dummiesBasePath, dummyPostController.Handle, []string{"test-token"})
	dummyPutController := dummyPutControllerType{}
	httpServer.AddRoute("PUT", dummiesBasePath+"/:id", dummyPutController.Handle, []string{"test-token"})
	dummyDeleteController := dummyDeleteControllerType{}
	httpServer.AddRoute("DELETE", dummiesBasePath+"/:id", dummyDeleteController.Handle, []string{"test-token"})

	// Listen
	go httpServer.Listen()
}

// testUnauthRequest tries to make an unauthorized request
func testUnauthRequest(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["unauthorized_request"]

	// Perform auth repo operation
	model := DummyJSONResource{HTTPBaseResource: HTTPBaseResource{"someid"}}
	err := httpDummiesRepository3.Create(&model)

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testFindBadRequest tries to get a resource without providing proper input data
func testFindBadRequest(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["bad_request_input_data"]

	// Perform repo operation against no id
	_, err := currentRepo.Find(7, 2)

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testFindNoResourcesRequest tries to get no resources with a valid input data
func testFindNoResourcesRequest(t *testing.T) {

	// Perform repo operation against a non existing property value
	rsc, err := currentRepo.Find("name=noname", 2)

	// Fail if error or any resource
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	} else if rsc == nil {
		t.Error("Expected resources collection, got nil")
	} else {

		// Try to parse resource or fail
		modelsPointer, ok := rsc.(*[]interface{})
		if !ok {
			t.Error("Expected to be able to parse resources collection, but could not")
		} else {
			models := *modelsPointer
			if len(models) > 0 {
				t.Errorf("Expected to get empty collection of resources, found %d", len(models))
			}
		}
	}
}

// testFindAllResourcesRequest tries to get all resources
func testFindAllResourcesRequest(t *testing.T) {

	// Define resources
	var resources []DummyJSONResource

	// Perform repo operation against all
	rsc, err := currentRepo.FindModels(&resources, nil, math.MaxInt32)

	// Fail if error or any resource
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	} else if rsc == nil {
		t.Error("Expected resources collection, got nil")
	} else {

		// Try to parse resource or fail
		modelsPointer, ok := rsc.(*[]DummyJSONResource)
		if !ok {
			t.Error("Expected to be able to parse resources collection, but could not")
		} else {
			models := *modelsPointer
			if len(models) != len(dummyJSONResourcesMap) {
				t.Errorf("Expected to get %d resources, found %d", len(dummyJSONResourcesMap), len(models))
			}
		}
	}
}

// testFindSortedResourcesRequest tries to get some resources sorted by field
func testFindSortedResourcesRequest(t *testing.T) {

	// Limit and sorting
	limit := 2
	sorting := "rank"

	// Define resources
	var resources []DummyJSONResource

	// Perform repo operation against all
	rsc, err := currentRepo.FindModels(&resources, nil, limit, sorting)

	// Fail if error or any resource
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	} else if rsc == nil {
		t.Error("Expected resources collection, got nil")
	} else {

		// Try to parse resource or fail
		modelsPointer, ok := rsc.(*[]DummyJSONResource)
		if !ok {
			t.Error("Expected to be able to parse resources collection, but could not")
		} else {
			models := *modelsPointer
			if len(models) != limit {
				t.Errorf("Expected to get %d resources, found %d", limit, len(models))
			} else {
				sorted := models[0].Rank
				for _, m := range models {
					if m.Rank < sorted {
						t.Errorf("Expected to have resources sorted by %s, which are not", sorting)
						return
					}
					sorted = m.Rank
				}
			}
		}
	}
}

// testGetBadRequest tries to get a resource without providing proper input data
func testGetBadRequest(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["bad_request_no_id"]

	// Perform repo operation against no id
	_, err := currentRepo.Get("")

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testMissingResource tries to get a missing resource
func testGetMissingResource(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["resource_not_found"]

	// Perform repo operation against inexistent id
	_, err := currentRepo.Get("blah")

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testGetExistingResource tries to get an existing resource
func testGetExistingResource(t *testing.T) {

	// Target id
	targetID := "my_id_2"

	// Resource model
	var resource DummyJSONResource

	// Perform repo operation against existent id
	rsc, err := currentRepo.GetModel(&resource, targetID)

	// Fail if error or no resource
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	} else if rsc == nil {
		t.Error("Expected resource, got nil")
	} else {

		// Try to parse resource or fail
		model, ok := rsc.(*DummyJSONResource)
		if !ok {
			t.Error("Expected to be able to parse resource, but could not")
		} else {
			if model.ID != targetID {
				t.Errorf("Expected resource with ID %s, got %s", targetID, model.ID)
			}
		}
	}
}

// testPostBadResource tries to create a resource providing invalid data
func testPostBadResource(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["bad_request"]

	// Perform repo operation with bad data
	model := DummyJSONResource{Name: "unidentifiable"}
	err := currentRepo.Create(&model)

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testPostValidResource tries to create a resource providing valid data
func testPostValidResource(t *testing.T) {

	// Perform repo operation with bad data
	model := DummyJSONResource{
		HTTPBaseResource: HTTPBaseResource{"dummy_991"},
		Name:             "hereIam",
		Rank:             3,
	}
	err := currentRepo.Create(&model)

	// Fail if error
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	}
}

// testUpdateBadResource tries to update an inexistent resource
func testUpdateBadResource(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["resource_not_found"]

	// Perform repo operation with bad data
	model := DummyJSONResource{HTTPBaseResource: HTTPBaseResource{"whatever"}}
	err := currentRepo.Update(map[string]interface{}{"id": "nowhereyouwillfindme"}, &model)

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testPostValidResource tries to update a created resource
func testUpdateCreatedResource(t *testing.T) {

	// Set a new name
	newName := "nowIamsomewherelese"

	// Perform repo operation with bad data
	model := DummyJSONResource{Name: newName}
	err := currentRepo.Update(map[string]interface{}{"name": "hereIam"}, &model)

	// Fail if  error
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	} else {
		if dummyJSONResourcesMap["dummy_991"].Name != newName {
			t.Error("Expected resource to get updated, but did not")
		}
	}
}

// testDeleteBadResource tries to delete an inexisting resource
func testDeleteBadResource(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["resource_not_found"]

	// Perform repo operation against existent id
	err := currentRepo.Delete("mehmehmeh")

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// testDeleteExistingResource tries to delete an existing resource
func testDeleteExistingResource(t *testing.T) {

	// Perform repo operation against existent id
	err := currentRepo.Delete("dummy_991")

	// Fail if error or no resource
	if err != nil {
		t.Errorf("Expected no errors, got %s", err.Error())
	}
}

// testFindBadResourceRequest tries to get a non existing resource
func testFindBadResourceRequest(t *testing.T) {

	// Extract expected response status
	expectedStr := expectedResponseStatusMap["resource_not_found"]

	// Perform repo operation against a non existing property value
	_, err := currentRepo.FindOne("id=dummy_991")

	// Fail if no error or different than expected
	if err == nil {
		t.Error("Expected error, got none")
	} else if err.Error() != expectedStr {
		t.Errorf("Expected %s, got %s", expectedStr, err.Error())
	}
}

// dummyTokenMiddleware requires a dummy token
func dummyTokenMiddleware(w http.ResponseWriter, r *http.Request, params url.Values) bool {

	// Get token from headers
	token := r.Header.Get("test-token")

	// Fail if empty
	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, `{"error":"Unauthorized"}`)
		return false
	}

	// Pass
	return true
}

type dummyFindControllerType struct {
	HTTPController
}

func (c *dummyFindControllerType) Handle(w http.ResponseWriter, r *http.Request, params url.Values) {

	// Init collections
	dummiesCol := []DummyJSONResource{}

	// Extract parameters
	id := params.Get("id")
	name := params.Get("name")
	rank, _ := strconv.Atoi(params.Get("rank"))
	sortings := params.Get("sort")
	limit, _ := strconv.Atoi(params.Get("limit"))

	// Loop over existing collection and filter stuff out
	for k, d := range dummyJSONResourcesMap {
		if id != "" && k != id {
			continue
		}
		if name != "" && d.Name != name {
			continue
		}
		if rank != 0 && d.Rank != rank {
			continue
		}
		dummiesCol = append(dummiesCol, d)
	}

	// Now sort if sorting defined for something
	// ACHTUNG! Only basic one-level increasing sorting here for tests
	if sortings == "id" {
		sort.SliceStable(dummiesCol, func(i, j int) bool { return dummiesCol[i].ID.(string) < dummiesCol[j].ID.(string) })
	}
	if sortings == "name" {
		sort.SliceStable(dummiesCol, func(i, j int) bool { return dummiesCol[i].Name < dummiesCol[j].Name })
	}
	if sortings == "rank" {
		sort.SliceStable(dummiesCol, func(i, j int) bool { return dummiesCol[i].Rank < dummiesCol[j].Rank })
	}

	// Limit the results, if defined
	if limit > 0 && len(dummiesCol) > limit {
		dummiesCol = dummiesCol[:limit]
	}

	// Return resource with 200
	c.HTTPJSONSuccess(w, dummiesCol, http.StatusOK)
}

type dummyGetControllerType struct {
	HTTPController
}

func (c *dummyGetControllerType) Handle(w http.ResponseWriter, r *http.Request, params url.Values) {

	// Get resource or 404
	rsc, ok := dummyJSONResourcesMap[params.Get("id")]
	if !ok {
		c.HTTPJSONError(w, "Not found", http.StatusNotFound)
		return
	}

	// Return resource with 200
	c.HTTPJSONSuccess(w, rsc, http.StatusOK)
}

type dummyPostControllerType struct {
	HTTPController
}

func (c *dummyPostControllerType) Handle(w http.ResponseWriter, r *http.Request, params url.Values) {

	// Parse incoming body
	decoder := json.NewDecoder(r.Body)
	var body DummyJSONResource
	err := decoder.Decode(&body)
	defer r.Body.Close()

	// If error decoding, it's a bad request 400
	if err != nil {
		c.HTTPJSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// If no ID is provided, it's also 400, we reduce tests complexity allowing to pass IDs here
	if body.ID == nil || body.ID == "" {
		c.HTTPJSONError(w, "No ID provided", http.StatusBadRequest)
		return
	}

	// Make sure the id is no there already
	for _, m := range dummyJSONResourcesMap {
		if m.ID == body.ID {
			c.HTTPJSONError(w, "Duplicate ID provided", http.StatusBadRequest)
			return
		}
	}

	// Add resource and send 201
	dummyJSONResourcesMap[body.ID.(string)] = body
	c.HTTPJSONSuccess(w, nil, http.StatusCreated)
}

type dummyPutControllerType struct {
	HTTPController
}

func (c *dummyPutControllerType) Handle(w http.ResponseWriter, r *http.Request, params url.Values) {

	// Get resource or 404
	rsc, ok := dummyJSONResourcesMap[params.Get("id")]
	if !ok {
		c.HTTPJSONError(w, "Not found", http.StatusNotFound)
		return
	}

	// Parse incoming body
	decoder := json.NewDecoder(r.Body)
	var body DummyJSONResource
	err := decoder.Decode(&body)
	defer r.Body.Close()

	// If error decoding, it's a bad request 400
	if err != nil {
		c.HTTPJSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Update whatever fields here
	if body.Name != "" {
		rsc.Name = body.Name
	}
	if body.Rank != 0 {
		rsc.Rank = body.Rank
	}

	// Update map
	dummyJSONResourcesMap[params.Get("id")] = rsc

	// Send 204
	c.HTTPJSONSuccess(w, nil, http.StatusNoContent)
}

type dummyDeleteControllerType struct {
	HTTPController
}

func (c *dummyDeleteControllerType) Handle(w http.ResponseWriter, r *http.Request, params url.Values) {

	// Get resource or 404
	rsc, ok := dummyJSONResourcesMap[params.Get("id")]
	if !ok {
		c.HTTPJSONError(w, "Not found", http.StatusNotFound)
		return
	}

	// Remove it from collection
	delete(dummyJSONResourcesMap, rsc.ID.(string))

	// Return 204
	c.HTTPJSONSuccess(w, nil, http.StatusNoContent)
}
