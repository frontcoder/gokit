package gobase

// TODO: These generic interfaces probably should be bestter in a different file, but which one?

// IField Global field interface
type IField interface {
	GetName() string
	// Add more stuff here if we intend to use fields outside DB paradigm
}

// Field Global field struct
type Field struct {
	Name string
}

// Enforce Interface implementation
var _ IField = (*Field)(nil)

// GetName IField interface implementation
func (f *Field) GetName() string {
	return f.Name
}

// IDBField Global database field interface
type IDBField interface {
	IField
	// Add more stuff here if we intend to use fields outside SQL paradigm
}

// DBField Global database field struct
type DBField struct {
	Field
}

// Enforce Interface implementation
var _ IDBField = (*DBField)(nil)

// IIndex Global index interface
type IIndex interface {
	GetName() string
	// Add more stuff here if we intend to use indexes outside DB paradigm
}

// Index Global index struct
type Index struct {
	Name string
}

// Enforce Interface implementation
var _ IIndex = (*Index)(nil)

// GetName IIndex interface implementation
func (i *Index) GetName() string {
	return i.Name
}

// IDBIndex Global database index interface
type IDBIndex interface {
	IIndex
	// Add more stuff here if we intend to use indexes outside SQL paradigm
}

// DBIndex Global database index struct
type DBIndex struct {
	Index
}

// Enforce Interface implementation
var _ IDBIndex = (*DBIndex)(nil)

// IEntity Global entity interface
type IEntity interface {
	GetName() string
	// Add more stuff here if we intend to use entities outside DB paradigm
}

// Entity Global entity struct
type Entity struct {
	Name string
}

// Enforce Interface implementation
var _ IEntity = (*Entity)(nil)

// GetName IEntity interface implementation
func (e *Entity) GetName() string {
	return e.Name
}

// IDBEntity Global database entity interface
type IDBEntity interface {
	IEntity
	GetFields() []IDBField
	// Add more stuff here if we intend to use entities outside SQL paradigm
}

// DBEntity Global database entity struct
type DBEntity struct {
	Entity
	Fields []IDBField
}

// Enforce Interface implementation
var _ IDBEntity = (*DBEntity)(nil)

// GetFields IDBEntity interface implementation
func (e *DBEntity) GetFields() []IDBField {
	return e.Fields
}

// IDBClient interface
type IDBClient interface {
	Connect() (interface{}, error)
	Disconnect()
	DropDatabase() error
	GetConnection() interface{}
	PingConnection() error
}

// DBConfig Global struct
type DBConfig struct {
	// Add here whatever is not implementation specific
}

// DBClient Global struct
type DBClient struct {
	// Add here whatever is not implementation specific
}

// IMigration interface
type IMigration interface {
	GetMigrationsEntityName() string
	GetKey() string
	GetCLient() IDBClient
	IsApplied() (bool, error)
	Execute() error
}

// DBMigration base structure
type DBMigration struct {
	MigrationsEntityName string
	Key                  string
	Client               IDBClient
}

// GetMigrationsEntityName returns the entity name associated to the migration (collection in noSQL, table in SQL)
func (m *DBMigration) GetMigrationsEntityName() string {
	return m.MigrationsEntityName
}

// GetKey returns the key associated to the migration (for incremental stuff)
func (m *DBMigration) GetKey() string {
	return m.Key
}

// GetCLient returns the client the migration uses
func (m *DBMigration) GetCLient() IDBClient {
	return m.Client
}

// IDBRepository Global database repository interface
type IDBRepository interface {
	IRepository
	Init()
	GetConnection() interface{}
	GetMainEntityName() string
	DropDatabase() error
}

// DBRepository Global db repository struct
type DBRepository struct {
	Repository
	MainEntityName string
}

// GetMainEntityName Gets main entity name
func (r *DBRepository) GetMainEntityName() string {
	return r.MainEntityName
}
