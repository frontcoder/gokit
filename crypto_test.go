package gobase

import (
	"fmt"
	"testing"
	"time"
)

// JWT test secret
var jwtTestSecret = "testjwtsecret"

// JWT test payload struct
type jwtTestPayloadType struct {
	Sub      string `json:"sub"`
	Iss      string `json:"iss"`
	Iat      int64  `json:"iat"`
	Exp      int64  `json:"exp"`
	HandleLc string `json:"H"`
	Email    string `json:"E"`
	Roles    string `json:"R"`
	Status   string `json:"S"`
}

// JWT test payload
var jwtTestPayload = jwtTestPayloadType{
	Sub:      "5b5daf784ff4d05609883bd4",
	Iss:      "mybrand",
	HandleLc: "testusr",
	Email:    "test@testing.go",
	Roles:    "masterracer admin",
	Status:   "bankrupt",
}

// We hold the encoded jwt here
var testEncodedJWT string

// TestCrypto performs the crypto tests
func TestCrypto(t *testing.T) {

	// Run some tests
	t.Run("jwt_encode hs256", testJWTEncode)
	t.Run("jwt_decode hs256 success", testJWTDecodeSuccess)
	time.Sleep(3 * time.Second)
	t.Run("jwt_decode hs256 expired", testJWTDecodeExpired)
}

// testJWTEncode encodes a payload into a JWT
func testJWTEncode(t *testing.T) {

	// Get current time, add to payload
	now := time.Now().Unix()
	jwtTestPayload.Iat = now
	jwtTestPayload.Exp = now + 2

	// Encode and throw error if any
	encoded, err := Crypto.JWTEncode(jwtTestPayload, jwtTestSecret, "hs256")
	if err != nil {
		t.Error(err)
	}
	testEncodedJWT = encoded
}

// testJWTDecodeSuccess succesfully decodes a payload into a JWT
func testJWTDecodeSuccess(t *testing.T) {

	// Decode and throw error if any
	var payload jwtTestPayloadType
	_, err := Crypto.JWTDecodePayload(testEncodedJWT, jwtTestSecret, &payload)
	if err != nil {
		t.Error(err)
		return
	}
	if &payload == nil {
		t.Error("Could not decode payload")
		return
	}
	// TODO: Check fields here?
}

// testJWTDecodeExpired decodes a payload into a JWT, but fails as it's expired
func testJWTDecodeExpired(t *testing.T) {

	// Decode and throw error if any
	var payload jwtTestPayloadType
	_, err := Crypto.JWTDecodePayload(testEncodedJWT, jwtTestSecret, &payload)
	if err == nil {
		t.Error("Token should have been expired", payload, time.Now().Unix())
		return
	}
	expectedSrrorString := "Expired token"
	if err.Error() != "Expired token" {
		t.Error(fmt.Sprintf("Expected error: %s, got: %s", expectedSrrorString, err.Error()))
		return
	}
}
