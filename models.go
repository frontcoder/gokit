package gobase

// Error Common error model
type Error struct {
	Message string
	Code    int
	Stack   string
}

// Response Common response model
type Response struct {
	Data  interface{}
	Error error
	Code  int
}

// ServiceMessage Common message model for communication with services
type ServiceMessage struct {
	RequestTarget   string
	RequestData     interface{}
	ResponseChannel chan *Response
}
