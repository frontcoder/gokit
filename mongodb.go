package gobase

import (
	"errors"
	"log"
	"strings"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// MongoDBIndex is the mongo version of it
// TODO: Make it compliant with Index interface
type MongoDBIndex struct {
	Keys            []string
	Unique          bool
	Sparse          bool
	Kind            string
	DecrementalSort bool
}

// MongoDBConfig MongoDB config model
type MongoDBConfig struct {
	DBConfig
	Hosts    []string `json:"hosts"`
	Database string   `json:"database"`
}

// FIXME: Switch to another library!!!

// MongoDBClient Global struct
type MongoDBClient struct {
	DBClient
	Config     MongoDBConfig
	Connection *mgo.Database
	Session    *mgo.Session
}

// Enforce Interface implementation
var _ IDBClient = (*MongoDBClient)(nil)

// Connect Connects and returns a MongoDB database connection
func (c *MongoDBClient) Connect() (interface{}, error) {

	// Get database configurationor use defaults
	dbHostsArr := c.Config.Hosts
	dbHostsStr := strings.Join(dbHostsArr, ",")
	if dbHostsStr == "" {
		dbHostsStr = "localhost"
	}
	dbName := c.Config.Database
	if dbName == "" {
		dbName = "test"
	}

	// Open DB session
	log.Println("Connecting to MongoDB server")
	db, err := mgo.Dial(dbHostsStr)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	c.Session = db

	// Switch the session to a monotonic behavior. It allows to read from replicas
	db.SetMode(mgo.Monotonic, true)

	// Set safe mode, we need all replicas to be on sync
	db.EnsureSafe(&mgo.Safe{
		W: len(dbHostsArr),
		J: true,
	})

	// Assign database connection
	log.Println("Connecting to database " + dbName)
	c.Connection = db.DB(dbName)
	if c == nil {
		return nil, errors.New("Could not connect to database " + dbName)
	}
	log.Println("Database connected")

	// Return no errors
	return c, nil
}

// Disconnect Closes and releases connection
func (c *MongoDBClient) Disconnect() {
	c.Connection.Session.Close()
}

// DropDatabase interface implementation
func (c *MongoDBClient) DropDatabase() error {
	return c.Connection.DropDatabase()
}

// GetConnection Gets inner database connection
func (c *MongoDBClient) GetConnection() interface{} {
	return c.Connection
}

// PingConnection Pings inner database connection
func (c *MongoDBClient) PingConnection() error {
	return c.Session.Ping()
}

// IMongoDBRepository Global database repository interface
type IMongoDBRepository interface {
	IDBRepository
	GetIndexes() []MongoDBIndex
	SetIndexes([]MongoDBIndex)
	EnsureIndexes(string) error
	GetObjectID(interface{}) (interface{}, error)
}

// MongoDBRepository Global mongodb repository struct
type MongoDBRepository struct {
	DBRepository
	Indexes []MongoDBIndex
	Client  *MongoDBClient
}

// Enforce Interface implementation
var _ IMongoDBRepository = (*MongoDBRepository)(nil)

// Init initializes stuff like indexes, etc.
// @override
func (r *MongoDBRepository) Init() {

	// Ensure model indexes, crash otherwise
	err := r.EnsureIndexes(r.GetMainEntityName())
	if err != nil {
		log.Panicln(err)
	}
}

// GetConnection Implementation of IDBInterface
// @override
func (r *MongoDBRepository) GetConnection() interface{} {
	return r.Client.GetConnection()
}

// GetIndexes Gets collection indexes
func (r *MongoDBRepository) GetIndexes() []MongoDBIndex {
	return r.Indexes
}

// SetIndexes Sets collection indexes
func (r *MongoDBRepository) SetIndexes(indexes []MongoDBIndex) {
	r.Indexes = indexes
}

// EnsureIndexes Implementation of IDBInterface
func (r *MongoDBRepository) EnsureIndexes(collectionName string) error {

	// If missing collection get default one
	if collectionName == "" {
		collectionName = r.GetMainEntityName()
	}

	// Get Indexes
	indexes := r.GetIndexes()

	// Get reference to collection
	collection := r.GetConnection().(*mgo.Database).C(collectionName)

	// Loop over regular indexes and ensure them
	for _, idx := range indexes {

		// Loop over index keys and prepend sort and kind if provided
		for i, k := range idx.Keys {
			if idx.DecrementalSort {
				k = "-" + k
			}
			if idx.Kind != "" {
				k = "$" + idx.Kind + k
			}
			idx.Keys[i] = k
		}

		// Buld a mongodb index
		index := mgo.Index{
			Key:        idx.Keys,
			Unique:     idx.Unique,
			Sparse:     idx.Sparse,
			Background: true,
		}

		// Ensure the index, fail if some couldn't get ensured
		err := collection.EnsureIndex(index)
		if err != nil {
			log.Printf("error assigning index %s\n", index.Key)
			return err
		}
	}

	// Alles gut!
	return nil
}

// DropDatabase interface implementation
func (r *MongoDBRepository) DropDatabase() error {
	return r.Client.DropDatabase()
}

// Find - IRepository implementation - Finds documents by mapped fields and sorting, limits et
// ACHTUNG! It returns bson.Raw data inside an interface, so not usable in practical scenarios, override it better
func (r *MongoDBRepository) Find(fields interface{}, limit int, sort ...string) (interface{}, error) {
	var documents []interface{}
	err := r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).Find(fields).Sort(sort...).Limit(limit).All(&documents)
	if documents == nil {
		documents = []interface{}{}
	}
	return documents, err
}

// FindOne - IRepository implementation - Finds single document by mapped fields and sorting
// ACHTUNG! It returns bson.Raw data inside an interface, so not usable in practical scenarios, override it better
func (r *MongoDBRepository) FindOne(fields interface{}, sort ...string) (interface{}, error) {
	objs, err := r.Find(fields, 1, sort...)
	if err != nil {
		return nil, err
	}
	docs, ok := objs.([]interface{})
	if !ok || len(docs) < 1 {
		return nil, errors.New("document not found")
	}
	return &docs[0], err
}

// Get - IRepository implementation - Gets document by primary identifier (_id)
// ACHTUNG! It returns bson.Raw data inside an interface, so not usable in practical scenarios, override it better
func (r *MongoDBRepository) Get(id interface{}) (interface{}, error) {
	objectID, err := r.GetObjectID(id)
	if err != nil {
		return nil, err
	}
	var doc interface{}
	err = r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).FindId(objectID).One(&doc)
	return &doc, err
}

// Create - IRepository implementation - Creates a document
// ACHTUNG! It just passes whatever receives, you may want to override and call it from there
func (r *MongoDBRepository) Create(in interface{}) error {
	if in == nil {
		return errors.New("No proper data to insert")
	}
	return r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).Insert(in)
}

// Update - IRepository implementation - Updates a document (by fields)
// ACHTUNG! It just passes whatever receives, you may want to override and call it from there
func (r *MongoDBRepository) Update(fields interface{}, in interface{}) error {
	if in == nil {
		return errors.New("Bad data for document update")
	}
	return r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).Update(fields, in)
}

// UpdateID - IRepository implementation - Updates a document (by _id)
// ACHTUNG! It just passes whatever receives, you may want to override and call it from there
func (r *MongoDBRepository) UpdateID(id interface{}, in interface{}) error {
	objectID, err := r.GetObjectID(id)
	if err != nil {
		return err
	}
	return r.Update(bson.M{"_id": objectID}, in)
}

// Delete - IRepository implementation - Deletes a document (by _id)
func (r *MongoDBRepository) Delete(id interface{}) error {
	objectID, err := r.GetObjectID(id)
	if err != nil {
		return err
	}
	return r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).RemoveId(objectID)
}

// GetObjectID is a helper that ensures we get an ObjectID from string (or an error)
func (r *MongoDBRepository) GetObjectID(id interface{}) (interface{}, error) {
	return GetObjectID(id)
}

// ObjectIDToString is a helper that converts an ObjectID to string (or an error)
func (r *MongoDBRepository) ObjectIDToString(id interface{}) (string, error) {
	return ObjectIDToString(id)
}

// GetDoc - Helper for getting a document by primary identifier (_id), but casting directly to passed doc
func (r *MongoDBRepository) GetDoc(doc interface{}, id interface{}) error {
	objectID, err := r.GetObjectID(id)
	if err != nil {
		return err
	}
	return r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).FindId(objectID).One(doc)
}

// FindDocs - Helper for finding documents by query, but casting directly to passed docs
func (r *MongoDBRepository) FindDocs(docs interface{}, fields interface{}, limit int, sort ...string) error {
	err := r.GetConnection().(*mgo.Database).C(r.GetMainEntityName()).Find(fields).Sort(sort...).Limit(limit).All(docs)
	if docs == nil {
		docs = []interface{}{}
	}
	return err
}

// GetObjectID is a helper that ensures we get an ObjectID from string (or an error)
func GetObjectID(id interface{}) (interface{}, error) {
	objectID, ok := id.(bson.ObjectId)
	if !ok || objectID == "" {
		idString, ok := id.(string)
		if !ok || idString == "" || !bson.IsObjectIdHex(idString) {
			return nil, errors.New("bad id provided")
		}
		objectID = bson.ObjectIdHex(idString)
	}
	return objectID, nil
}

// ObjectIDToString is a helper that converts an ObjectID to string (or an error)
func ObjectIDToString(id interface{}) (string, error) {
	objectID, ok := id.(bson.ObjectId)
	if !ok || objectID == "" {
		idString, ok := id.(string)
		if !ok || idString == "" || !bson.IsObjectIdHex(idString) {
			return "", errors.New("bad ObjectID provided")
		}
		return idString, nil
	}
	return objectID.Hex(), nil
}
