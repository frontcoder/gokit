package gobase

// IComponent Component Interface
type IComponent interface {
	Init()
	GetApp() *Application
	GetRepositories() map[string]IRepository
	GetDefaultRepository() string
	GetServices() map[string]IService
	GetHTTPControllers() map[string]IHTTPController
	GetWSControllers() map[string]IWSController
}

// Component struct containing core stuff in every component
type Component struct {
	App               *Application
	Repositories      map[string]IRepository
	DefaultRepository string
	Services          map[string]IService
	HTTPControllers   map[string]IHTTPController
	WSControllers     map[string]IWSController
}

// GetRepositories gets the defined repositories for the component
func (c *Component) GetRepositories() map[string]IRepository {
	return c.Repositories
}

// GetDefaultRepository gets the defined default repository for the component
func (c *Component) GetDefaultRepository() string {
	return c.DefaultRepository
}

// GetServices gets the defined services for the component
func (c *Component) GetServices() map[string]IService {
	return c.Services
}

// GetHTTPControllers gets the defined http controllers for the component
func (c *Component) GetHTTPControllers() map[string]IHTTPController {
	return c.HTTPControllers
}

// GetWSControllers gets the defined websocket controllers for the component
func (c *Component) GetWSControllers() map[string]IWSController {
	return c.WSControllers
}

// GetApp gets the underlying app
func (c *Component) GetApp() *Application {
	return c.App
}
