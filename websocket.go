package gobase

import (
	"errors"
	"golang.org/x/net/websocket"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// WSMessage structure for all needed message info
type WSMessage struct {
	ID    string      `json:"id"`
	Path  string      `json:"path"`
	Data  interface{} `json:"data"`
	Error *Error      `json:"error"`
}

// IWSConnection Websocket WSConnection interface
type IWSConnection interface {
	Open(*websocket.Conn)
	Close() error
	Receive(interface{}) error
	Send(interface{}) error
	GetConnection() *websocket.Conn
	SetConnection(*websocket.Conn)
	GetRouter() IWSRouter
	SetRouter(IWSRouter)
	GetClientID() string
	SetClientID(string)
	GetConnectionType() string
	SetConnectionType(string)
	GetLastPingTime() time.Time
	SetLastPingTime(time.Time)
}

// WSConnection Global structure for identifying a single ws connection
type WSConnection struct {
	ClientID     string
	Type         string
	Conn         *websocket.Conn
	router       IWSRouter
	LastPingTime time.Time
}

// Enforce Interface implementation
var _ IWSConnection = (*WSConnection)(nil)

// Open is the main Websocket handler once upgraded
func (c *WSConnection) Open(conn *websocket.Conn) {

	// Assign connection object
	c.SetConnection(conn)

	// Here we place a LOOP but for checking we get Pongs on Pings
	c.SetLastPingTime(time.Now())
	go c.pingLoop()

	// Set router main listener for the LOOP!
	router := c.GetRouter()
	if router != nil {
		router.Listen(c)
	}
}

// Close tries to close the websocket connection
func (c *WSConnection) Close() error {
	return c.GetConnection().Close()
}

// Receive a websocket message in JSON format
func (c *WSConnection) Receive(msg interface{}) error {
	return websocket.JSON.Receive(c.GetConnection(), msg)
}

// Send a websocket message in JSON format
func (c *WSConnection) Send(msg interface{}) error {
	return websocket.JSON.Send(c.GetConnection(), msg)
}

// GetConnection is the underlying connection getter
func (c *WSConnection) GetConnection() *websocket.Conn {
	return c.Conn
}

// SetConnection is the underlying connection setter
func (c *WSConnection) SetConnection(conn *websocket.Conn) {
	c.Conn = conn
}

// GetRouter is the underlying router getter
func (c *WSConnection) GetRouter() IWSRouter {
	return c.router
}

// SetRouter is the underlying router setter
func (c *WSConnection) SetRouter(router IWSRouter) {
	c.router = router
}

// GetClientID is the Client ID getter
func (c *WSConnection) GetClientID() string {
	return c.ClientID
}

// SetClientID is the Client ID setter
func (c *WSConnection) SetClientID(clientID string) {
	c.ClientID = clientID
}

// GetConnectionType is the connection type getter
func (c *WSConnection) GetConnectionType() string {
	return c.Type
}

// SetConnectionType is the connection type setter
func (c *WSConnection) SetConnectionType(connType string) {
	c.Type = connType
}

// GetLastPingTime gets the last time a Ping was succesfully reponded with a Pong
func (c *WSConnection) GetLastPingTime() time.Time {
	return c.LastPingTime
}

// SetLastPingTime sets the last time a Ping was succesfully reponded with a Pong
func (c *WSConnection) SetLastPingTime(t time.Time) {
	c.LastPingTime = t
}

// pingLoop sends PINGs every X seconds, also checks proper PONG received, otherwise it closes the connection
func (c *WSConnection) pingLoop() {

	// Infinite loop
	for {

		// Try to send PING message
		if err := c.Send(WSMessage{
			Path: "ping",
		}); err != nil {
			log.Printf("[WS] [WARN] Could not send PING message to connection %s from %s", c.GetConnectionType(), c.GetClientID())
			break
		}

		// Check when was last received pong, and if more than ... 20 seconds ... break loop
		if time.Now().After(c.GetLastPingTime().Add(20 * time.Second)) {
			break
		}

		// Wait some time for next one (5 seconds seems the standard)
		time.Sleep(5 * time.Second)
	}

	// Whenever we exit this loop, close connection
	if err := c.Close(); err != nil {
		log.Printf("[WS] [WARN] Could not close connection %s from %s after missing PONGs", c.GetConnectionType(), c.GetClientID())
	}
}

// IWSRouter Websocket WSRouter interface
type IWSRouter interface {
	Listen(IWSConnection)
	AddMiddleware(string, WSMiddleware)
	AddRoute(string, WSHandler, []string)
}

// wsNode represents a struct of each wsnode in the router tree
type wsNode struct {
	children      []*wsNode
	pathComponent string
	deepness      int
	handler       WSHandler
	middlewares   []string
}

// WSRouter is ... the router!
type WSRouter struct {
	tree             *wsNode
	defaultWSHandler WSHandler
	middlewares      map[string]WSMiddleware
}

// Enforce Interface implementation
var _ IWSRouter = (*WSRouter)(nil)

// NewWSRouter creates a new router, using a default fallback handler
func NewWSRouter(rootWSHandler WSHandler) *WSRouter {

	// Initialize root wsnode
	node := wsNode{
		pathComponent: "",
		deepness:      0,
		handler:       nil,
		middlewares:   nil,
	}

	// Create and return router with root wsnode and default handler
	return &WSRouter{
		tree:             &node,
		defaultWSHandler: rootWSHandler,
		middlewares:      make(map[string]WSMiddleware),
	}
}

// AddMiddleware adds a named middleware to the router
func (r *WSRouter) AddMiddleware(name string, middleware WSMiddleware) {
	r.middlewares[name] = middleware
}

// AddRoute takes a method, a pattern, and an websocket handler for a route
func (r *WSRouter) AddRoute(path string, handler WSHandler, middlewares []string) {

	// Empty paths not allowed
	if len(path) < 1 {
		log.Panic("Empty paths not allowed")
		return
	}

	// Remove leading/trailing colons if any
	path = strings.TrimLeft(path, ":")
	path = strings.TrimRight(path, ":")

	// Empty paths not allowed
	if len(path) < 1 {
		log.Panic("Empty paths not allowed")
		return
	}

	// Add wsnode to the tree
	r.tree.addNode(path, handler, middlewares)
}

// Listen Listens for Websocket messages
func (r *WSRouter) Listen(conn IWSConnection) {

	// Endless loop for messages
	for {

		// Try to get a message in JSON format
		var msg WSMessage
		err := conn.Receive(&msg)

		// Fatal error
		if err != nil {

			// Just try to close connection
			if err2 := conn.Close(); err2 != nil {
				// TODO: More meaningful logs here?
				log.Println("[WS] [WARN] Could not properly close websocket connection")
				break
			}
		}

		// Extract path, strip trailing slash if not the only one
		path := msg.Path

		// Empty paths not allowed
		if len(path) < 1 {
			log.Println("[WS] [WARN] Received empty path in message")
			continue
		}

		// Remove leading/trailing colons if any
		path = strings.TrimLeft(path, ":")
		path = strings.TrimRight(path, ":")

		// Empty paths not allowed
		if len(path) < 1 {
			log.Println("[WS] [WARN] Received empty path in message")
			continue
		}

		// Create the path components slice
		pathComponents := strings.Split(path, ":")
		count := len(pathComponents)

		// Special case, PONG message, just update last time connection got one
		if (count == 1) && (pathComponents[0] == "pong") {
			conn.SetLastPingTime(time.Now())
			continue
		}

		// Traverse routes wsnodes with the splitted path entries and parsed params
		// If no wsnode, apply default handler
		node, _, _ := r.tree.traverse(pathComponents, 0)
		if node == nil {
			r.defaultWSHandler(&msg, conn)
			continue
		}

		// Get wsnode handler for method, if there's none, apply default handler
		handler := node.handler
		if handler == nil {
			r.defaultWSHandler(&msg, conn)
			continue
		}

		// Check if the last path component is the one we request, otherwise apply default handler
		if (count > 0) && ((node.pathComponent != pathComponents[count-1]) || count > node.deepness) {
			r.defaultWSHandler(&msg, conn)
			continue
		}

		// Process here wsnode middlewares if set, ending if any doesn't pass
		middlewares := node.middlewares
		if middlewares != nil {
			for _, middleware := range middlewares {
				pass := r.middlewares[middleware](&msg, conn)
				if !pass {
					conn.Send(WSMessage{
						ID:   msg.ID,
						Path: msg.Path,
						Error: &Error{
							Message: "Could not pass middleware " + middleware,
							Code:    403,
						},
					})
					continue
				}
			}
		}

		// At this point there's a wsnode which has an http handler for requested method, then just forward to it
		handler(&msg, conn)
	}
}

// addNode - adds a wsnode to the routes tree tree
// Will add multiple wsnodes if path can be broken up into multiple components
// Those wsnodes will have no handler implemented and will fall through to the default handler
func (n *wsNode) addNode(path string, handler WSHandler, middlewares []string) {

	// Normalize middlewares
	if middlewares == nil {
		middlewares = []string{}
	}

	// Get a slice with all the components that form the path (separated by slashes - /)
	pathComponents := strings.Split(path, ":")
	pathComponentsLength := len(pathComponents)

	// As long as there's a path, process it
	for pathComponentsLength > 0 {

		// Traverse router wsnodes using the path components, try to get a wsnode and it's final path
		aNode, pathComponent, iterations := n.traverse(pathComponents, 0)

		// If done more than one iteration, discount it, making sure not below 0
		pathComponentsLength = len(pathComponents) - iterations
		if pathComponentsLength < 0 {
			pathComponentsLength = 0
		}

		// A wsnode exists for this path, update its handler for the defined method and end here
		if (aNode.pathComponent == pathComponent) && (pathComponentsLength == 0) {
			aNode.handler = handler
			aNode.middlewares = middlewares
			return
		}

		// Create a new wsnode for this new path
		newNode := wsNode{
			pathComponent: pathComponent,
			deepness:      iterations + 1,
			handler:       nil,
			middlewares:   nil,
		}

		// If this is the last part of the path, add to the new wsnode the handler
		if pathComponentsLength == 0 {
			newNode.handler = handler
			newNode.middlewares = middlewares
		}

		// Append created wsnode to the current retrieved one
		aNode.children = append(aNode.children, &newNode)
	}
}

// traverse moves along the tree
// It returns the wsnode and component found.
func (n *wsNode) traverse(pathComponents []string, iterations int) (*wsNode, string, int) {

	// Get first path component
	pathComponent := pathComponents[0]

	// Check if children routes wsnodes
	if len(n.children) > 0 {

		// Loop over wsnode children
		for _, child := range n.children {

			// Check if child wsnode owns that path OR has named parameter
			if pathComponent == child.pathComponent {

				// Screw current path component and get the remaining ones
				nextPathComponents := pathComponents[1:]

				// If remaining path components, recurse
				if len(nextPathComponents) > 0 {
					return child.traverse(nextPathComponents, iterations+1)
				}

				// No more path components, so return this chld wsnode and the current path component
				return child, pathComponent, iterations + 1
			}
		}
	}

	// Return current wsnode and path component, as no children found for that path
	return n, pathComponent, iterations
}

// WSHandler is just like a "golang.org/x/net/websocket" WSHandler
type WSHandler func(*WSMessage, IWSConnection)

// WSMiddleware is just like a "golang.org/x/net/websocket" WSHandler, but returns true/false if passed it or not
type WSMiddleware func(*WSMessage, IWSConnection) bool

// IWSServer Websocket WSServer (Listener) interface
type IWSServer interface {
	Listen()
	AddMiddleware(string, WSMiddleware)
	AddRoute(string, WSHandler, []string)
}

// WSServerConfig Websocket Listener config model
type WSServerConfig struct {
	Path       string
	HTTPServer IHTTPServer
}

// WSServer Global struct
type WSServer struct {
	Config        WSServerConfig
	WSRouter      IWSRouter
	WSConnections map[string]map[string]IWSConnection
}

// Enforce Interface implementation
var _ IWSServer = (*WSServer)(nil)

// Listen Sets up and starts the Websocket server (listener)
func (s *WSServer) Listen() {

	// Fail if no HTTP WSServer provided
	if s.Config.HTTPServer == nil {
		log.Fatal(errors.New("No HTTP server provided to Websocket listener"))
	}

	// Set a defaut path if not set
	if s.Config.Path == "" {
		s.Config.Path = "/ws"
	}

	// Check if no router defined, then create it
	if s.WSRouter == nil {
		s.initWSRouter()
	}

	// Start connections map if not done yet
	if s.WSConnections == nil {
		s.WSConnections = make(map[string]map[string]IWSConnection)
	}

	// Start listening for Websocket connections
	// TODO: Add default middlewares???
	s.Config.HTTPServer.AddRoute("GET", s.Config.Path+"/:connection", s.httpHandler, nil)
	log.Printf("Websocket WSServer listening on path %s\n", s.Config.Path)
}

// AddMiddleware Adds a middleware to Websocket listener (router in fact)
func (s *WSServer) AddMiddleware(name string, middleware WSMiddleware) {

	// Check if no router defined, then create it
	if s.WSRouter == nil {
		s.initWSRouter()
	}

	// Add the middleware
	s.WSRouter.AddMiddleware(name, middleware)
}

// AddRoute Adds a route to Websocket listener
func (s *WSServer) AddRoute(path string, handler WSHandler, middlewares []string) {

	// Check if no router defined, then create it
	if s.WSRouter == nil {
		s.initWSRouter()
	}

	// Add the route
	s.WSRouter.AddRoute(path, handler, middlewares)
}

// initWSRouter creates a new Websocket message WSRouter with default handler fallback
func (s *WSServer) initWSRouter() {

	// Set default handler
	// TODO: Make this less verbose and show only path? Or do somethingÇ
	defaultWSHandler := func(msg *WSMessage, conn IWSConnection) {
		log.Printf("[WS] [WARN] Non-routed message path %#v for %s from %s\n", msg, conn.GetConnectionType(), conn.GetClientID())
	}

	// Create router with defatul handler
	s.WSRouter = NewWSRouter(defaultWSHandler)
}

// httpHandler is the main HTTP handler that upgrades to Websocket
func (s *WSServer) httpHandler(w http.ResponseWriter, r *http.Request, params url.Values) {

	// TODO: Comment this out if testing from outside
	proto := "http"
	if r.TLS != nil {
		proto = proto + "s"
	}
	if r.Header.Get("Origin") != (proto + "://" + r.Host) {
		http.Error(w, "Origin not allowed", http.StatusForbidden)
		return
	}

	// Get ClientID from somewhere
	// TODO: Get it from headers/cookies/sessions or wherever is more suitable
	clientID := r.RemoteAddr
	if clientID == "" {
		http.Error(w, "Remote address could not be parsed", http.StatusBadRequest)
		return
	}

	// Get connection type from url, default to something
	connectionType := params.Get("connection")
	if connectionType == "" {
		connectionType = "default"
	}

	// Create connection object
	conn := &WSConnection{
		ClientID: clientID,
		Type:     connectionType,
		Conn:     nil,
		router:   s.WSRouter,
	}

	// Add to connections pool, create Client pool if it doesn't exist yet
	if s.WSConnections[clientID] == nil {
		s.WSConnections[clientID] = make(map[string]IWSConnection)
	}
	s.WSConnections[clientID][connectionType] = conn

	// Now do the upgrade
	websocket.Handler(conn.Open).ServeHTTP(w, r)
}

// IWSController Websocket WSController interface
type IWSController interface {
	Handle(*WSMessage, IWSConnection)
	GetComponent() IComponent
	Send(IWSConnection, string, string, interface{}, *Error)
}

// WSController Websocket WSController base struct
type WSController struct {
	Component IComponent
}

// GetComponent gets the underlying component, if any (as interface)
func (c *WSController) GetComponent() IComponent {
	return c.Component
}

// Send is a wrapper for sending a message
func (c *WSController) Send(conn IWSConnection, id string, path string, data interface{}, err *Error) {
	if err := conn.Send(WSMessage{
		ID:    id,
		Path:  path,
		Data:  data,
		Error: err,
	}); err != nil {
		log.Printf("[WSController] Error: %v", err)
	}
}
