package gobase

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

// TestEnv defines the global variable accessible by all tests (e.g. environment parsed file)
var TestEnv Environment

// TestMain is the entrypoint to all tests
func TestMain(m *testing.M) {

	// Call flag.Parse() here if need to use them
	var fileName string
	flag.StringVar(&fileName, "env", "env.json", "File to use as environment for tests. See env.sample.json for an example.")
	flag.Parse()

	// Get environment configuration from file system (JSON)
	cfgRaw, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Printf("Could not load environment file for tests: %s\n", err.Error())
		os.Exit(1)
	}

	// Parse configuration
	err = FromJSON(&TestEnv, cfgRaw)
	if err != nil {
		log.Printf("Could not parse environment file for tests: %s\n", err.Error())
		os.Exit(1)
	}

	// Run tests and exit
	os.Exit(m.Run())
}
