package gobase

import (
	"errors"
	"fmt"
	"io"
	"log"
	"testing"
)

// LoggerMockWriter testing struct
type LoggerMockWriter struct {
	stack []string
}

// Write interface method implementation, really simplistic
func (w *LoggerMockWriter) Write(in []byte) (int, error) {

	// Add data to stack, previous cast to string
	w.stack = append(w.stack, string(in[:]))

	// Return required stuff by interface (not really relevant here)
	return len(in), nil
}

// Ensure Writer interface is implemented by LoggerMockWriter
var _ io.Writer = (*LoggerMockWriter)(nil)

// logInput structure for tests
type logInput struct {
	level string
	text  string
	args  []interface{}
}

// Writer to use in tests (mock)
var loggerWriter = LoggerMockWriter{
	stack: []string{},
}

// logger instance to use
var logger ISlackLogger

// loggerConfigLevels to use
var loggerConfigLevels = []string{"debug", "info", "warn", "error"}

// defaultLoggerSlackEndpoint to use
var defaultLoggerSlackEndpoint = "https://hooks.slack.com/services/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"

// loggerInputs to use
var loggerInputs = []logInput{
	{"debug", "", nil},
	{"debug", "no params", nil},
	{"debug", "hey yo %s", []interface{}{"man"}},
	{"info", "nothing new!", []interface{}{}},
	{"info", "I'm done %s", []interface{}{"mate"}},
	{"warn", "nothing to see here!", []interface{}{}},
	{"warn", "hey I'm back %s", []interface{}{"dude"}},
	{"error", "something went wrong, code: %d", []interface{}{404}},
	{"error", "something went wrong, stack: %v", []interface{}{map[string]string{"first": "one"}}},
}

// loggerOutputs to expect
var loggerOutputs = map[string][]string{
	"debug": {
		"[debug] no params\n",
		"[debug] hey yo man\n",
		"[info] nothing new!\n",
		"[info] I'm done mate\n",
		"[warn] nothing to see here!\n",
		"[warn] hey I'm back dude\n",
		"[error] something went wrong, code: 404\n",
		"[error] something went wrong, stack: map[first:one]\n",
	},
	"info": {
		"[info] nothing new!\n",
		"[info] I'm done mate\n",
		"[warn] nothing to see here!\n",
		"[warn] hey I'm back dude\n",
		"[error] something went wrong, code: 404\n",
		"[error] something went wrong, stack: map[first:one]\n",
	},
	"warn": {
		"[warn] nothing to see here!\n",
		"[warn] hey I'm back dude\n",
		"[error] something went wrong, code: 404\n",
		"[error] something went wrong, stack: map[first:one]\n",
	},
	"error": {
		"[error] something went wrong, code: 404\n",
		"[error] something went wrong, stack: map[first:one]\n",
	},
}

// loadLoggerEnv reads environment for these tests from globally parsed environment file
func loadLoggerEnv() (*LoggerConfig, error) {

	// Parse configuration
	config := TestEnv.Logger
	if config == nil {
		return nil, errors.New("Missing Logger environment configuration")
	}

	// Return no error
	return config, nil
}

// TestLogger performs the logger tests
func TestLogger(t *testing.T) {

	// Try to load env config or load defaults
	config, err := loadLoggerEnv()
	if err != nil {
		log.Printf("Could not load environment for tests: %s\n", err.Error())
		config = &LoggerConfig{Slack: nil}
	}
	if config.Slack == nil {
		config.Slack = &SlackConfig{Endpoint: ""}
	}
	if config.Slack.Endpoint == "" {
		log.Println("Using default slack endpoint for tests")
		config.Slack.Endpoint = defaultLoggerSlackEndpoint
	}

	// Test logger to use
	logger = &SlackLogger{
		Logger{
			Config: *config,
		},
	}

	// Loop over different level configs
	for _, v := range loggerConfigLevels {

		// Add custom level/loggerWriter/prefix/flags to logger, empty its stack first
		logger.SetOutput(&loggerWriter)
		logger.SetLevel(v)
		loggerWriter.stack = []string{}

		// Run grouped tests for this logger config
		t.Run(v, func(t *testing.T) {
			t.Run("logs", testLogs)
			t.Run("slack log", testSlackLog)
		})
	}
}

// testLogs logs to stdout
func testLogs(t *testing.T) {

	// Loop over loggerInputs and log them
	for _, in := range loggerInputs {
		logger.Log(in.level, in.text, in.args...)
	}

	// Loop over output and verify them
	for i, out := range loggerOutputs[logger.GetLevel()] {
		stackOut := loggerWriter.stack[i]
		if stackOut != out {
			t.Errorf("Expected %s, got %s", out, stackOut)
		}
	}
}

// testSlackLogs tests logs to Slack
func testSlackLog(t *testing.T) {

	// Loop over loggerInputs and send them, we just check for no errors here
	// TODO: Check even further on slack? Is it worth it? Change user/emoji/etc?
	for _, in := range loggerInputs {
		str := fmt.Sprintf(in.text, in.args...)
		if err := logger.Send(in.level, str); err != nil {
			t.Error(err.Error())
		}
	}
}
