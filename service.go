package gobase

// IService Service general interface
type IService interface {
	GetComponent() IComponent
	Call(interface{}) *Response
}

// Service struct containing core stuff in every service
type Service struct {
	Component IComponent
}

// GetComponent gets the underlying component, if any (as interface)
func (s *Service) GetComponent() IComponent {
	return s.Component
}

// AsyncCall acts as a wrapper for the underlying interface method for global package access
func AsyncCall(s IService, in interface{}, fn func(*Response)) {
	fn(Call(s, in))
}

// ChannelCall acts as a wrapper for the underlying interface method for global package access
func ChannelCall(s IService, in interface{}, c chan *Response) {
	c <- Call(s, in)
}

// Call acts as a wrapper for the underlying interface method for global package access
func Call(s IService, in interface{}) *Response {
	return s.Call(in)
}
