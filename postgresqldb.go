package gobase

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"regexp"
	"strings"

	_ "github.com/lib/pq" // We don't need more than just the driver
)

// IPostgreSQLDBField Global PostgreSQL database field interface
type IPostgreSQLDBField interface {
	IDBField
	GetType() string
	IsNullable() bool
}

// PostgreSQLDBField Global database field struct
type PostgreSQLDBField struct {
	DBField
	Type     string
	Nullable bool
}

// Enforce Interface implementation
var _ IPostgreSQLDBField = (*PostgreSQLDBField)(nil)

// GetType IPostgreSQLDBField interface implementation
func (f *PostgreSQLDBField) GetType() string {
	return f.Type
}

// IsNullable IPostgreSQLDBField interface implementation
func (f *PostgreSQLDBField) IsNullable() bool {
	return f.Nullable
}

// IPostgreSQLDBIndex Global PostgreSQL database index interface
type IPostgreSQLDBIndex interface {
	IDBIndex
	GetType() string
	IsUnique() bool
	GetFields() []IPostgreSQLDBField
}

// PostgreSQLDBIndex Global database field struct
type PostgreSQLDBIndex struct {
	DBIndex
	Type   string
	Unique bool
	Fields []IPostgreSQLDBField
}

// Enforce Interface implementation
var _ IPostgreSQLDBIndex = (*PostgreSQLDBIndex)(nil)

// GetType IPostgreSQLDBIndex interface implementation
func (i *PostgreSQLDBIndex) GetType() string {
	return i.Type
}

// IsUnique IPostgreSQLDBIndex interface implementation
func (i *PostgreSQLDBIndex) IsUnique() bool {
	return i.Unique
}

// GetFields IPostgreSQLDBIndex interface implementation
func (i *PostgreSQLDBIndex) GetFields() []IPostgreSQLDBField {
	return i.Fields
}

// IPostgreSQLDBEntity Global PostgreSQL database entity interface
type IPostgreSQLDBEntity interface {
	IDBEntity
	GetTableName() string
}

// PostgreSQLDBEntity Global PostgreSQL database entity struct
type PostgreSQLDBEntity struct {
	DBEntity
	TableName string
}

// Enforce Interface implementation
var _ IPostgreSQLDBEntity = (*PostgreSQLDBEntity)(nil)

// GetTableName IDBEntity interface implementation
func (e *PostgreSQLDBEntity) GetTableName() string {
	return e.TableName
}

// PostgreSQLDBConfig PostgreSQL DB config model
type PostgreSQLDBConfig struct {
	DBConfig
	Hosts    []string `json:"hosts"`
	Database string   `json:"database"`
	User     string   `json:"user"`
	Password string   `json:"password"`
}

// PostgreSQLDBClient Global struct
type PostgreSQLDBClient struct {
	DBClient
	Config     PostgreSQLDBConfig
	Connection *sql.DB
}

// Enforce Interface implementation
var _ IDBClient = (*PostgreSQLDBClient)(nil)

// Connect Connects and returns a PostgreSQL DB database connection
func (c *PostgreSQLDBClient) Connect() (interface{}, error) {

	// TODO: Parse cluster/HA config here and use it!

	// Shortcuts
	dbName := c.Config.Database

	// Create connection or fail
	log.Printf("Connecting to PostgreSQL database %s", dbName)
	connStr := "postgres://" + c.Config.User + ":" + c.Config.Password + "@" + c.Config.Hosts[0] + "/" + dbName + "?sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Printf("Could not connect to PostgreSQL database %s, %v", dbName, err)
		return nil, err
	}
	log.Printf("PostgreSQL database %s connected", dbName)

	// Add to Connection property
	c.Connection = db

	// Return no errors
	return db, nil
}

// Disconnect Closes and releases connection
func (c *PostgreSQLDBClient) Disconnect() {
	err := c.Connection.Close()
	if err != nil {
		log.Printf("Error closing PostgreSQL database connection %v", err)
	}
}

// DropDatabase interface implementation
func (c *PostgreSQLDBClient) DropDatabase() error {
	// TODO:
	return nil
}

// GetConnection Gets inner database connection
func (c *PostgreSQLDBClient) GetConnection() interface{} {
	return c.Connection
}

// PingConnection Pings inner database connection
func (c *PostgreSQLDBClient) PingConnection() error {
	return c.Connection.Ping()
}

// PostgreSQLMigration structure
type PostgreSQLMigration struct {
	DBMigration
	SQLScript string
}

// Enforce Interface implementation
var _ IMigration = (*PostgreSQLMigration)(nil)

// IsApplied checks if migrations is already applied or not
// @override
func (m *PostgreSQLMigration) IsApplied() (bool, error) {

	// Get connection or fail
	conn, err := m.GetClientConnection()
	if err != nil {
		return false, err
	}

	// Query the migration and fail if error or nothing returned
	rows, err := conn.Query("SELECT * FROM ? WHERE key = '?';", m.GetMigrationsEntityName(), m.GetKey())
	if err != nil {
		return false, err
	}

	// Return result
	return rows.Next(), nil
}

// Execute a migration
// @override
func (m *PostgreSQLMigration) Execute() error {

	// Let's make sure we got the table first
	if err := m.ensureMigrationsTable(); err != nil {
		return err
	}

	// Check it's not already applied, otherwise just discard it
	isApplied, err := m.IsApplied()
	if err != nil {
		return err
	}
	if isApplied {
		log.Printf("Migration %s was already applied", m.GetKey())
		return nil
	}

	// Get connection or fail
	conn, err := m.GetClientConnection()
	if err != nil {
		return err
	}

	// Start a transaction or fail
	tx, err := conn.Begin()
	if err != nil {
		return err
	}

	// Execute SQL script or fail
	res, err := tx.Exec(m.SQLScript)
	if err != nil {
		_ = tx.Rollback()
		return err
	}
	if res == nil {
		_ = tx.Rollback()
		return fmt.Errorf("Migration %s failed to execute?", m.GetKey())
	}

	// Now insert the migration fact into its table
	res, err = tx.Exec("INSERT INTO ? (key) VALUES ('?');", m.GetMigrationsEntityName(), m.GetKey())
	if err != nil {
		_ = tx.Rollback()
		return err
	}
	if res == nil {
		_ = tx.Rollback()
		return fmt.Errorf("Migration %s failed to register itself?", m.GetKey())
	}

	// Return the result of committing it
	return tx.Commit()
}

// GetClientConnection Check and gets a proper connection or error
func (m *PostgreSQLMigration) GetClientConnection() (*sql.DB, error) {

	// Get raw client or fail client
	rawClient := m.GetCLient()
	if rawClient == nil {
		return nil, fmt.Errorf("Migration %s with no client defined", m.GetKey())
	}

	// Cast client or fail
	client, ok := rawClient.(*PostgreSQLDBClient)
	if !ok {
		return nil, fmt.Errorf("Could not cast client for migration %s", m.GetKey())
	}

	// Get underlying connection or fail
	rawConn := client.GetConnection()
	if rawConn == nil {
		return nil, fmt.Errorf("Migration %s client with no connection defined", m.GetKey())
	}

	// Cast connection or fail
	conn, ok := rawConn.(*sql.DB)
	if !ok {
		return nil, fmt.Errorf("Could not cast client connection for migration %s", m.GetKey())
	}

	// Ensure connection is live
	if err := client.PingConnection(); err != nil {
		return nil, fmt.Errorf("Could not verify client connection live status for migration %s", m.GetKey())
	}

	// Return the connection
	return conn, nil
}

// ensureMigrationsTable ensures the migrations table is setup
func (m *PostgreSQLMigration) ensureMigrationsTable() error {

	// Get connection or fail
	conn, err := m.GetClientConnection()
	if err != nil {
		return err
	}

	// Execute SQL script or fail
	res, err := conn.Exec("CREATE TABLE ? IF NOT EXISTS (id BIGSERIAL PRIMARY KEY, key VARCHAR(128) UNIQUE);", m.GetMigrationsEntityName())
	if err != nil {
		return err
	}
	if res == nil {
		return fmt.Errorf("Migration %s failed to ensure migrations table?", m.GetKey())
	}

	// Should be created, right?
	return nil
}

// StringFieldRegexp serves to check whether we need to enclose values into quotes or not
var StringFieldRegexp = regexp.MustCompile(`(?i)^(CHAR|VARCHAR|NVARCHAR|STRING|TEXT).*$`)

// IPostgreSQLDBRepository Global database repository interface
type IPostgreSQLDBRepository interface {
	IDBRepository
	GetMainEntity() IPostgreSQLDBEntity
	EnsureTable(IPostgreSQLDBEntity) error
}

// PostgreSQLDBRepository Global PostgreSQL DB repository struct
type PostgreSQLDBRepository struct {
	DBRepository
	Connection *sql.DB
	Context    *context.Context
	MainEntity IPostgreSQLDBEntity
}

// Enforce Interface implementation
var _ IPostgreSQLDBRepository = (*PostgreSQLDBRepository)(nil)

// GetConnection Implementation of IDBInterface
// @override
func (r *PostgreSQLDBRepository) GetConnection() interface{} {
	return r.Connection
}

// DropDatabase interface implementation
func (r *PostgreSQLDBRepository) DropDatabase() error {
	// TODO:
	return nil
}

// GetMainEntity gets the main entity for this repository
func (r *PostgreSQLDBRepository) GetMainEntity() IPostgreSQLDBEntity {
	return r.MainEntity
}

// Init initializes stuff like indexes, etc.
// @override
func (r *PostgreSQLDBRepository) Init() {

	// Ensure table for main entity (this can be overwritten per component repo, of course)
	r.EnsureTable(r.GetMainEntity())
}

// EnsureTable Implementation of IPostgreSQLDBInterface
func (r *PostgreSQLDBRepository) EnsureTable(entity IPostgreSQLDBEntity) error {

	// Get underlying connection or die
	db, ok := r.GetConnection().(*sql.DB)
	if !ok {
		return errors.New("Could not cast database connection to SQL one")
	}

	// If missing structure get default one
	if entity == nil {
		entity = r.GetMainEntity()
		if entity == nil {
			return errors.New("No entity passed to EnsureTable and no main entty defined")
		}
	}

	// First of all, try to get current status of the table, if any
	stmtStr := "select column_name, data_type, character_maximum_length from INFORMATION_SCHEMA.COLUMNS where table_name = '?'"
	rows, err := db.QueryContext(*r.Context, stmtStr, entity.GetTableName())
	if err != nil {
		return err
	}

	// TODO:
	log.Printf("Returned entity structure rows: %v", rows)

	return errors.New("TO IMPLEMENT")
}

// Find - IRepository implementation - Finds rows by query and sorting, limits et
// This differs from initial implementation (done for MongoDB) in that what is passed is the full text query
// @override
func (r *PostgreSQLDBRepository) Find(query interface{}, limit int, sort ...string) (interface{}, error) {

	// Get underlying connection or die
	db, ok := r.GetConnection().(*sql.DB)
	if !ok {
		return nil, errors.New("Could not cast database connection to SQL one")
	}

	// Start statements string
	stmtStr := "SELECT * FROM ?"

	// Try to parse incoming query string, adding it if not empty
	if query != nil {
		queryStr, ok := query.(string)
		if !ok {
			return nil, errors.New("Could not cast database query to SQL one")
		}
		if len(queryStr) > 0 {
			stmtStr = stmtStr + " WHERE " + queryStr
		}
	}

	// Try to parse sort and fill in query
	if len(sort) > 0 {
		stmtStr = stmtStr + " ORDER BY"
		for _, sorting := range sort {
			dir := "ASC"
			if strings.HasPrefix(sorting, "-") {
				dir = "DESC"
			}
			stmtStr = stmtStr + " " + sorting + " " + dir + ","
		}
		stmtStr = strings.TrimRight(stmtStr, ",")
	}

	// Try to parse limit and fill in query
	if limit > 0 {
		stmtStr = fmt.Sprintf(stmtStr+" LIMIT %d", limit)
	}

	// Execute query or fail
	rows, err := db.QueryContext(*r.Context, stmtStr, r.GetMainEntityName())
	if err != nil {
		return nil, err
	}
	return rows, nil
}

// FindOne - IRepository implementation - Finds single row by mapped fields and sorting
// @override
func (r *PostgreSQLDBRepository) FindOne(query interface{}, sort ...string) (interface{}, error) {

	// Delegate into generic Find method, with a limit of exactly 1
	rows, err := r.Find(query, 1, sort...)
	if err != nil {
		return nil, err
	}

	// Get into rows and expect only one or fail
	entries, ok := rows.([]interface{})
	if !ok || len(entries) < 1 {
		return nil, errors.New("row not found")
	}
	return &entries[0], err
}

// Get - IRepository implementation - Gets row by primary identifier (id)
// @override
func (r *PostgreSQLDBRepository) Get(id interface{}) (interface{}, error) {

	// Parse id into BIGINT or fail
	nid, ok := id.(int64)
	if !ok {
		return nil, errors.New("bad id provided")
	}

	// Fill query string and delegate into FindOne
	query := fmt.Sprintf("id = %d", nid)
	return r.FindOne(query)
}

// Create - IRepository implementation - Creates a row
// @override
func (r *PostgreSQLDBRepository) Create(in interface{}) error {

	// Get underlying connection or die
	db, ok := r.GetConnection().(*sql.DB)
	if !ok {
		return errors.New("Could not cast database connection to SQL one")
	}

	// Get the sql tag mappings for the passed struct
	fieldsMap, err := ToCustomTagMap(in, "sql")
	if err != nil {
		return err
	}

	// Init fields and values strings
	fieldsInsertStr := "("
	valuesInsertStr := "("

	// Loop over the tag fields map
	for fKey, fVal := range fieldsMap {

		// Reference to current field properties map
		currentFiledMap, ok := fVal.(map[string]interface{})
		if !ok {
			return errors.New("Could not cast map from field tags map inside " + fKey)
		}

		// Reference to the field name in DB
		fieldName, ok := currentFiledMap["field"]
		if !ok {
			return errors.New("Could not map sql field tag in " + fKey)
		}
		fieldNameStr, ok := fieldName.(string)
		if !ok {
			return errors.New("Could not cast string from sql field tag in " + fKey)
		}

		// Reference to the field type in DB
		fieldType, ok := currentFiledMap["type"]
		if !ok {
			return errors.New("Could not map sql type tag in " + fKey)
		}
		fieldTypeStr, ok := fieldType.(string)
		if !ok {
			return errors.New("Could not cast string from sql type tag in " + fKey)
		}

		// Determine whether its a text field which should be quoted
		useQuotes := StringFieldRegexp.MatchString(fieldTypeStr)

		// Use util to fetch the real value in the field
		val := GetFieldByName(in, fKey)

		// Parse it into a string , hmmmmm
		// FIXME: What about other types? Need to fix this
		valStr, ok := val.(string)
		if !ok {
			return errors.New("Could not cast string from value in " + fKey)
		}

		// Add quotes if needed
		if useQuotes {
			valStr = "'" + valStr + "'"
		}

		// Append to fields and values strings
		fieldsInsertStr = fieldsInsertStr + fieldNameStr + ","
		valuesInsertStr = valuesInsertStr + valStr + ","
	}

	// TRim trailing comma in both strings
	fieldsInsertStr = strings.TrimRight(fieldsInsertStr, ",") + ")"
	valuesInsertStr = strings.TrimRight(valuesInsertStr, ",") + ")"

	// Create final statement and run the query or fail
	stmtStr := "INSERT INTO ? " + fieldsInsertStr + " VALUES " + valuesInsertStr + ";"
	result, err := db.ExecContext(*r.Context, stmtStr, r.GetMainEntityName())
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		return errors.New("Could not create entry")
	}
	return nil
}

// Update - IRepository implementation - Updates a row (by fields)
// @override
func (r *PostgreSQLDBRepository) Update(query interface{}, in interface{}) error {

	// Get underlying connection or die
	db, ok := r.GetConnection().(*sql.DB)
	if !ok {
		return errors.New("Could not cast database connection to SQL one")
	}

	// Get query string or fail
	queryStr := ""
	if query != nil {
		qStr, ok := query.(string)
		if !ok {
			return errors.New("Could not cast database query to SQL one")
		}
		queryStr = queryStr + qStr
	}

	// Get the sql tag mappings for the passed struct
	fieldsMap, err := ToCustomTagMap(in, "sql")
	if err != nil {
		return err
	}

	// Init the fields update string
	fieldsSetStr := ""

	// Loop over fields
	for fKey, fVal := range fieldsMap {

		// Reference to current field tags
		currentFiledMap, ok := fVal.(map[string]interface{})
		if !ok {
			return errors.New("Could not cast map from field tags map inside " + fKey)
		}

		// Reference to field name in DB
		fieldName, ok := currentFiledMap["field"]
		if !ok {
			return errors.New("Could not map sql field tag in " + fKey)
		}
		fieldNameStr, ok := fieldName.(string)
		if !ok {
			return errors.New("Could not cast string from sql field tag in " + fKey)
		}

		// Reference to field type in DB
		fieldType, ok := currentFiledMap["type"]
		if !ok {
			return errors.New("Could not map sql type tag in " + fKey)
		}
		fieldTypeStr, ok := fieldType.(string)
		if !ok {
			return errors.New("Could not cast string from sql type tag in " + fKey)
		}

		// Check whether to use quotes or not for a field update
		useQuotes := StringFieldRegexp.MatchString(fieldTypeStr)

		// Get real field value
		val := GetFieldByName(in, fKey)

		// Parse it into a string , hmmmmm
		// FIXME: What about other types? Need to fix this
		valStr, ok := val.(string)
		if !ok {
			return errors.New("Could not cast string from value in " + fKey)
		}

		// Add quotes if needed
		if useQuotes {
			valStr = "'" + valStr + "'"
		}

		// Append to update string
		fieldsSetStr = fieldsSetStr + fieldNameStr + " = " + valStr + ","
	}

	// Trim trailing comma
	fieldsSetStr = strings.TrimRight(fieldsSetStr, ",")

	// Create statement and run it or fail
	stmtStr := "UPDATE ? SET " + fieldsSetStr
	if len(queryStr) > 0 {
		stmtStr = stmtStr + " WHERE " + queryStr
	}
	result, err := db.ExecContext(*r.Context, stmtStr, r.GetMainEntityName())
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows < 1 {
		return errors.New("Could not update entries for query " + queryStr)
	}
	return nil
}

// UpdateID - IRepository implementation - Updates a row (by _id)
// @override
func (r *PostgreSQLDBRepository) UpdateID(id interface{}, in interface{}) error {

	// Try to parse the BIGINT or fail
	idInt, ok := id.(int64)
	if !ok {
		return errors.New("Could not cast id to a BIGINT (int64)")
	}

	// Generate query string and delegate into general Update method
	idQueryStr := fmt.Sprintf("id = %d", idInt)
	return r.Update(idQueryStr, in)
}

// Delete - IRepository implementation - Deletes a row (by _id)
// @override
func (r *PostgreSQLDBRepository) Delete(id interface{}) error {

	// Get underlying connection or die
	db, ok := r.GetConnection().(*sql.DB)
	if !ok {
		return errors.New("Could not cast database connection to SQL one")
	}

	// Try to parse the BIGINT or fail
	idInt, ok := id.(int64)
	if !ok {
		return errors.New("Could not cast id to a BIGINT (int64)")
	}

	// Generate query string
	idQueryStr := fmt.Sprintf("id = %d", idInt)

	// Create statement, run query or fail
	stmtStr := "DELETE FROM ? WHERE " + idQueryStr
	result, err := db.ExecContext(*r.Context, stmtStr, r.GetMainEntityName())
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		return errors.New("Could not delete entry for " + idQueryStr)
	}
	return nil
}
