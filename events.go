package gobase

import (
	"net/http"
	"strings"
)

// ServiceEventsHandler global core service events handler
type ServiceEventsHandler struct {
	Channel    chan *ServiceMessage
	Components map[string]IComponent
}

// Listen Start listening into channel
func (h *ServiceEventsHandler) Listen() {
	for msg := range h.Channel {
		go h.Handle(msg)
	}
}

// Handle Processes incoming messages into channel
func (h *ServiceEventsHandler) Handle(message *ServiceMessage) {

	// Get output channel for response
	ch := message.ResponseChannel

	// Get the target service or fail
	serviceString := message.RequestTarget
	if serviceString == "" {
		ch <- BuildResponse(nil, "no target service provided", http.StatusBadRequest)
		return
	}

	// Split target service into component:service
	aServiceString := strings.Split(serviceString, ":")
	if len(aServiceString) < 2 {
		ch <- BuildResponse(nil, "no proper target service", http.StatusBadRequest)
		return
	}

	// Get component from target string
	componentName := aServiceString[0]
	if (componentName == "") || (h.Components[componentName] == nil) {
		ch <- BuildResponse(nil, "no proper service component", http.StatusBadRequest)
		return
	}
	component := h.Components[componentName]

	// Get service from component
	serviceName := aServiceString[1]
	if (serviceName == "") || (component.GetServices() == nil) || (component.GetServices()[serviceName] == nil) {
		ch <- BuildResponse(nil, "service not found in component", http.StatusNotFound)
		return
	}
	service := component.GetServices()[serviceName]

	// Call service and forward to channel
	ch <- service.Call(message.RequestData)
}
