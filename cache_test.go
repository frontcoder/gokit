package gobase

import (
	"errors"
	"strings"
	"testing"
	"time"
)

// CacheKeyData structure for testing messages
type cacheKeyData struct {
	Value      string
	Expiration time.Duration
	Set        bool
	SetAt      time.Time
}

// cacheKeyExpiration is the expiration to use for tests
var cacheKeyExpiration = 10 * time.Second

// cacheKeySearches are the key searches we'll use for tests
var cacheKeySearches = map[string]bool{
	"test":              false,
	"test*":             true,
	"testkey01":         true,
	"wontmatchanything": false,
}

// cacheData is the data we'll use for tests
var cacheData = map[string]*cacheKeyData{
	"testkey01": {
		Value: "permanent",
	},
	"testkey02": {
		Value:      "temporary",
		Expiration: cacheKeyExpiration,
	},
}

// Global variables in the test scope
var redisConfig *RedisConfig
var redisClient ICacheClient

// loadCacheEnv reads environment for these tests from globally parsed environment file
func loadCacheEnv() (*RedisConfig, error) {

	// Parse configuration
	config := TestEnv.RedisCache
	if config == nil {
		return nil, errors.New("Missing Redis environment configuration")
	}

	// Return no error
	return config, nil
}

// TestCache is the main test loop
func TestCache(t *testing.T) {

	// Load environment configuration or fail
	var err error
	redisConfig, err = loadCacheEnv()
	if err != nil {
		t.Error(err.Error())
		return
	}

	// Create test client
	redisClient = &RedisClient{Config: *redisConfig}

	// Run tests, adding some delays in between
	if b := t.Run("connect", testCacheConnect); !b {
		t.Error(errors.New("Could not connect client"))
		return
	}
	t.Run("set keys/values", testCacheSet)
	t.Run("list keys", testCacheKeys)
	t.Run("get key values immediately", testCacheGet)
	time.Sleep(cacheKeyExpiration + 1*time.Second)
	t.Run("get key values post-expiration", testCacheGet)
	t.Run("delete keys", testCacheDel)
	t.Run("get key values post-deletion", testCacheGet)
	t.Run("disconnect", testCacheDisconnect)
}

// testCacheClientConnect tests the client connection
func testCacheClientConnect(t *testing.T, c ICacheClient) {
	if _, err := c.Connect(); err != nil {
		t.Error(err.Error())
	}
}

// testCacheConnect tests the cache connection
func testCacheConnect(t *testing.T) {
	testCacheClientConnect(t, redisClient)
}

// testCacheClientDisconnect tests the client disconnection
func testCacheClientDisconnect(t *testing.T, c ICacheClient) {
	if err := c.Disconnect(); err != nil {
		t.Error(err.Error())
	}
}

// testCacheDisconnect tests the cache disconnection
func testCacheDisconnect(t *testing.T) {
	testCacheClientDisconnect(t, redisClient)
}

// testCacheSet tests a value is properly set into cache (looping over input data)
func testCacheSet(t *testing.T) {
	for k, v := range cacheData {
		if err := redisClient.Set(k, v.Value, v.Expiration); err != nil {
			t.Error(err.Error())
		}
		cacheData[k].Set = true
		cacheData[k].SetAt = time.Now()
	}
}

// testCacheClientGet tests a value is properly retrieved from client
// it covers whether it should be expired or not
func testCacheClientGet(t *testing.T, c ICacheClient) {
	for k, v := range cacheData {
		var deltaTime time.Duration
		if v.Set {
			deltaTime = time.Since(v.SetAt)
		}
		val, err := c.Get(k)
		if err != nil {
			if v.Set {
				if v.Expiration <= 0 {
					t.Error(err.Error())
				}
				if deltaTime < v.Expiration {
					t.Error(errors.New("Could not get key " + k + " even when it should not be expired yet"))
				}
			}
		} else {
			if !v.Set {
				t.Error(errors.New("Should not get key " + k + " as it should have not been set ever"))
			}
			if (v.Expiration > 0) && (deltaTime > v.Expiration) {
				t.Error(errors.New("Should not get key " + k + " as it should have been expired already"))
			}
			if val != v.Value {
				t.Error(errors.New("Expected " + v.Value + ", got " + val.(string) + " for key " + k))
			}
		}
	}
}

// testCacheGet tests a value is properly retrieved from cache
// it covers getting value from other hosts when in failover setup
func testCacheGet(t *testing.T) {
	testCacheClientGet(t, redisClient)
	clientConfig := redisClient.(*RedisClient).Config
	if clientConfig.MasterName != "" {
		for _, h := range clientConfig.Hosts {
			standaloneClient := &RedisClient{
				Config: RedisConfig{
					Hosts: []string{strings.Replace(h, "26379", "6379", 1)},
				},
			}
			testCacheClientConnect(t, standaloneClient)
			testCacheClientGet(t, standaloneClient)
			testCacheClientDisconnect(t, standaloneClient)
		}
	}
}

// testCacheDel tests a value is properly deleted from cache
func testCacheDel(t *testing.T) {
	for k := range cacheData {
		if err := redisClient.Del(k); err != nil {
			t.Error(err.Error())
		}
		cacheData[k].Set = false
	}
}

// testCacheKeys tests keys are properly retrieved from cache
// it also covers whenever they should not be retrieved
func testCacheKeys(t *testing.T) {
	for k, r := range cacheKeySearches {
		matches, err := redisClient.Keys(k)
		if err != nil {
			t.Error(err.Error())
		}
		hasMatches := (len(matches) > 0)
		if r != hasMatches {
			txt := " not "
			if r {
				txt = " "
			}
			t.Error("Key " + k + " should" + txt + "have matches")
		}
	}
}
