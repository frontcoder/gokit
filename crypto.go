package gobase

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"hash"
	"strings"
	"time"
)

// The main struct for this mini-package
type crypto struct{}

// Crypto is the exported variable for the mini-package
var Crypto = &crypto{}

// Standard JWT Header as per RFC 7519 spec
type jwtHeader struct {
	Alg string `json:"alg"`
	Typ string `json:"typ"`
}

// defaultJWTHeader we'll consider, for using signatures using a single secret
var defaultJWTHeader = jwtHeader{
	Alg: "HS256",
	Typ: "JWT",
}

// Standard expiration payload used in most sites, just for checking the expiration date
// There's no way we can ensure any payload structure  at this point
type jwtExpirationPayload struct {
	Exp int64 `json:"exp"`
}

// Interface we'll use to pass to Hash
type hmacAlg interface {
	Hash() func() hash.Hash
}

// HS256 HMAC Algorithm implementation
type hs256Alg struct{}

// Interface compliant
func (a *hs256Alg) Hash() func() hash.Hash {
	return sha256.New
}

// Base64Encode takes in a string and returns a base 64 encoded string
func (c *crypto) Base64Encode(src string) string {
	return strings.TrimRight(base64.URLEncoding.EncodeToString([]byte(src)), "=")
}

// Base64Decode takes in a base 64 encoded string and returns the actual string or an error of it fails to decode the string
func (c *crypto) Base64Decode(src string) (string, error) {

	// Fill it to 64 if not done
	if l := len(src) % 4; l > 0 {
		src += strings.Repeat("=", 4-l)
	}

	// Decode the string, fail if errors
	decoded, err := base64.URLEncoding.DecodeString(src)
	if err != nil {
		errMsg := fmt.Errorf("Decoding Error %s", err)
		return "", errMsg
	}

	// Return decoded string
	return string(decoded), nil
}

// Hash generates a Hmac256 hash of a string using a secret
func (c *crypto) Hash(src string, secret string, alg string) string {

	// Create a byte slice from the string
	key := []byte(secret)

	// Declare an interface for the algorith we'll use
	var currHmacAlg hmacAlg

	// Decide which algorith implementation to use depending on the received parameter
	switch alg {
	// TODO: Add more algorithms here
	case "HS256":
		currHmacAlg = &hs256Alg{}
	default:
		currHmacAlg = &hs256Alg{}
	}

	// Run the algorith when creating the hash
	h := hmac.New(currHmacAlg.Hash(), key)
	h.Write([]byte(src))

	// Return its base 64 string representation
	return c.Base64Encode(string(h.Sum(nil)))
}

// IsValidHash validates a hash againt a value
func (c *crypto) IsValidHash(value string, hash string, secret string, alg string) bool {
	return hash == c.Hash(value, secret, alg)
}

// JWTDecode Will try to decode a JWT token, verifying its signature first
func (c *crypto) JWTDecode(jwt string, secret string) (interface{}, error) {

	// Declare an empty interface variable
	var dummy interface{}

	// Delegate into the payload based one but with the dummy interface
	return c.JWTDecodePayload(jwt, secret, &dummy)
}

// JWTDecodePayload Will try to decode a JWT token, verifying its signature first, into the provided payload interface
func (c *crypto) JWTDecodePayload(jwt string, secret string, payload interface{}) (interface{}, error) {

	// Split the token, there should be 3 parts
	token := strings.Split(jwt, ".")

	// Ensure the jwt token contains header, payload and token, otherwise throw error
	if len(token) != 3 {
		return nil, errors.New("Invalid token: token should contain header, payload and secret")
	}

	// Declare, decode and parse Header, get Algorith from first part string
	header := jwtHeader{}
	decodedHeader, err := c.Base64Decode(token[0])
	if err != nil {
		return nil, fmt.Errorf("Invalid header: %s", err.Error())
	}
	err = json.Unmarshal([]byte(decodedHeader), &header)
	if err != nil {
		return nil, fmt.Errorf("Invalid header: %s", err.Error())
	}
	alg := header.Alg

	// Deecode and parse payload from second part string
	decodedPayload, err := c.Base64Decode(token[1])
	if err != nil {
		return nil, fmt.Errorf("Invalid payload: %s", err.Error())
	}
	err = json.Unmarshal([]byte(decodedPayload), payload)
	if err != nil {
		return nil, fmt.Errorf("Invalid payload: %s", err.Error())
	}

	// Checks if the token has expired as long as we can verify it has expiration field
	var expPayload jwtExpirationPayload
	err = json.Unmarshal([]byte(decodedPayload), &expPayload)
	if err == nil && expPayload.Exp > 0 {
		now := time.Now().Unix()
		if now > expPayload.Exp {
			return nil, errors.New("Expired token")
		}
	}

	// Build the signature and verify it
	signatureValue := token[0] + "." + token[1]
	if !c.IsValidHash(signatureValue, token[2], secret, alg) {
		return nil, errors.New("Invalid token")
	}

	// Return the payload, token is verified
	return payload, nil
}

// Encode generates a JWT token given a payload, secret and hashing algorithm
func (c *crypto) JWTEncode(payload interface{}, secret string, alg string) (string, error) {

	// Assign default algorith if not specified
	if alg == "" {
		alg = defaultJWTHeader.Alg
	}

	// Create a header with algorithm and type (which is JWT in this case)
	headerPayload := jwtHeader{
		Alg: alg,
		Typ: defaultJWTHeader.Typ,
	}

	// JSON + Base64 encode header into a string
	str, err := json.Marshal(headerPayload)
	if err != nil {
		return "", fmt.Errorf("Could not marshal header. Error: %s", err.Error())
	}
	base64Header := c.Base64Encode(string(str))

	// JSON + Base64 encode payload into a string
	encodedPayload, err := json.Marshal(payload)
	if err != nil {
		return "", fmt.Errorf("Could not marshal payload. Error: %s", err.Error())
	}
	base64Payload := c.Base64Encode(string(encodedPayload))

	// Create signature concatenating these 2 strings
	signatureValue := base64Header + "." + base64Payload

	// Create signature hash using the algorithm
	signatureHash := c.Hash(signatureValue, secret, alg)

	// Now return the full concatenation
	return signatureValue + "." + signatureHash, nil
}
