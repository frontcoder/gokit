package gobase

// IRepository Global repository interface, with very basic CRUD for main entity
type IRepository interface {
	Find(interface{}, int, ...string) (interface{}, error)
	FindOne(interface{}, ...string) (interface{}, error)
	Get(interface{}) (interface{}, error)
	Create(interface{}) error
	Update(interface{}, interface{}) error
	UpdateID(interface{}, interface{}) error
	Delete(interface{}) error
}

// Repository global struct
type Repository struct {
	// Nothing here so far, add whatever is nor DB specific?
}
