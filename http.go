package gobase

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// HTTPHandler is just like a "net/http" HTTPHandler, but with params
type HTTPHandler func(http.ResponseWriter, *http.Request, url.Values)

// HTTPMiddleware is just like a "net/http" HTTPHandler, but returns true/false if passed it or not
type HTTPMiddleware func(http.ResponseWriter, *http.Request, url.Values) bool

// httpRouteNode represents a struct of each node in the router tree
type httpRouteNode struct {
	children      []*httpRouteNode
	pathComponent string
	isNamedParam  bool
	deepness      int
	methods       map[string]HTTPHandler
	middlewares   map[string][]string
}

// HTTPRouter is ... the router!
type HTTPRouter struct {
	tree               *httpRouteNode
	defaultHTTPHandler HTTPHandler
	staticHTTPHandlers map[string]HTTPHandler
	middlewares        map[string]HTTPMiddleware
}

// NewHTTPRouter creates a new router, using a default fallback handler
func NewHTTPRouter(rootHTTPHandler HTTPHandler) *HTTPRouter {

	// Initialize root node
	node := httpRouteNode{
		pathComponent: "/",
		isNamedParam:  false,
		deepness:      0,
		methods:       make(map[string]HTTPHandler),
		middlewares:   make(map[string][]string),
	}

	// Create and return router with root node and default handler
	return &HTTPRouter{
		tree:               &node,
		defaultHTTPHandler: rootHTTPHandler,
		staticHTTPHandlers: make(map[string]HTTPHandler),
		middlewares:        make(map[string]HTTPMiddleware),
	}
}

// AddMiddleware adds a named middleware to the router
func (r *HTTPRouter) AddMiddleware(name string, middleware HTTPMiddleware) {
	r.middlewares[name] = middleware
}

// AddStaticHandler adds a static handler to the router (for loading assets mainly)
func (r *HTTPRouter) AddStaticHandler(name string, handler HTTPHandler) {
	r.staticHTTPHandlers[name] = handler
}

// AddRoute takes a method, a pattern, and an http handler for a route
func (r *HTTPRouter) AddRoute(method, path string, handler HTTPHandler, middlewares []string) {

	// Empty paths not allowed
	length := len(path)
	if length < 1 {
		log.Panic("Empty paths not allowed")
		return
	}

	// Fail if path doesn't tart by slash (/)
	if path[0] != '/' {
		log.Panic("Path has to start with a /")
		return
	}

	// Remove trailing slash if more than one path components (so no '/api/something/', but allow '/')
	if length > 1 {
		path = strings.TrimRight(path, "/")
	}

	// Add node to the tree
	r.tree.addNode(method, path, handler, middlewares)
}

// ServeHTTP "net/http" package server interface implementation
func (r *HTTPRouter) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	// Parse incoming data from request
	req.ParseForm()

	// Extract parameters from request
	params := req.Form

	// Extract path, strip trailing slash if not the only one
	path := req.URL.Path
	if len(path) > 1 {
		path = strings.TrimRight(path, "/")
	}

	// Create the path components slice
	pathComponents := strings.Split(path, "/")[1:]
	count := len(pathComponents)

	// Check if it's static
	if count > 0 && r.staticHTTPHandlers[pathComponents[0]] != nil {
		r.staticHTTPHandlers[pathComponents[0]](w, req, params)
		return
	}

	// Traverse routes nodes with the splitted path entries and parsed params
	// If no node, apply default handler
	node, _, _ := r.tree.traverse(pathComponents, params, 0)
	if node == nil {
		r.defaultHTTPHandler(w, req, params)
		return
	}

	// Get node handler for method, if there's none, apply default handler
	handler := node.methods[req.Method]
	if handler == nil {
		r.defaultHTTPHandler(w, req, params)
		return
	}

	// Check if the last path component is the one we request, otherwise apply default handler
	if (count > 0) && ((!node.isNamedParam) && (node.pathComponent != pathComponents[count-1]) || count > node.deepness) {
		r.defaultHTTPHandler(w, req, params)
		return
	}

	// Process here node middlewares if set, ending if any doesn't pass
	middlewares := node.middlewares[req.Method]
	if middlewares != nil {
		for _, middleware := range middlewares {
			middlewareFunc := r.middlewares[middleware]
			if middlewareFunc != nil {
				pass := r.middlewares[middleware](w, req, params)
				if !pass {
					return
				}
			}
		}
	}

	// At this point there's a node which has an http handler for requested method, then just forward to it
	handler(w, req, params)
}

// addNode - adds a node to the routes tree tree
// Will add multiple nodes if path can be broken up into multiple components
// Those nodes will have no handler implemented and will fall through to the default handler
func (n *httpRouteNode) addNode(method, path string, handler HTTPHandler, middlewares []string) {

	// Normalize middlewares
	if middlewares == nil {
		middlewares = []string{}
	}

	// Get a slice with all the components that form the path (separated by slashes - /)
	pathComponents := strings.Split(path, "/")[1:]
	pathComponentsLength := len(pathComponents)

	// As long as there's a path, process it
	for pathComponentsLength > 0 {

		// Traverse router nodes using the path components, try to get a node and it's final path
		aNode, pathComponent, iterations := n.traverse(pathComponents, nil, 0)

		// If done more than one iteration, discount it, making sure not below 0
		pathComponentsLength = len(pathComponents) - iterations
		if pathComponentsLength < 0 {
			pathComponentsLength = 0
		}

		// A node exists for this path, update its handler for the defined method and end here
		// FIXME: Check this commented out thing, maybe we need to retrieve deepness in the traverse
		if (aNode.pathComponent == pathComponent) && (pathComponentsLength == 0) {
			aNode.methods[method] = handler
			aNode.middlewares[method] = middlewares
			return
		}

		// Create a new node for this new path
		newNode := httpRouteNode{
			pathComponent: pathComponent,
			isNamedParam:  false,
			deepness:      iterations + 1,
			methods:       make(map[string]HTTPHandler),
			middlewares:   make(map[string][]string),
		}

		// Check if this path component is a named parameter, checking the symbol we use (:)
		if len(pathComponent) > 0 && pathComponent[0] == ':' {
			newNode.isNamedParam = true
		}

		// If this is the last part of the path, add to the new node the handler
		if pathComponentsLength == 0 {
			newNode.methods[method] = handler
			newNode.middlewares[method] = middlewares
		}

		// Append created node to the current retrieved one
		aNode.children = append(aNode.children, &newNode)
	}
}

// traverse moves along the tree adding named params as it comes and across them
// It returns the node and component found.
func (n *httpRouteNode) traverse(pathComponents []string, params url.Values, iterations int) (*httpRouteNode, string, int) {

	// Get first path component
	pathComponent := pathComponents[0]

	// Check if children routes nodes
	if len(n.children) > 0 {

		// Loop over node children
		for _, child := range n.children {

			// Check if child node owns that path OR has named parameter
			if pathComponent == child.pathComponent || child.isNamedParam {

				// If it's a named parameter, add it to params
				if child.isNamedParam && params != nil {
					params.Add(child.pathComponent[1:], pathComponent)
				}

				// Screw current path component and get the remaining ones
				nextPathComponents := pathComponents[1:]

				// If remaining path components, recurse
				if len(nextPathComponents) > 0 {
					return child.traverse(nextPathComponents, params, iterations+1)
				}

				// No more path components, so return this chld node and the current path component
				return child, pathComponent, iterations + 1
			}
		}
	}

	// Return current node and path component, as no children found for that path
	return n, pathComponent, iterations
}

// IHTTPServer Interface
type IHTTPServer interface {
	Listen()
	AddMiddleware(string, HTTPMiddleware)
	AddRoute(string, string, HTTPHandler, []string)
	AddStaticHandler(string, HTTPHandler)
}

// HTTPServerConfig HTTP HTTPServer config model
type HTTPServerConfig struct {
	Port uint16 `json:"port"`
}

// HTTPServer Global struct
type HTTPServer struct {
	Config     HTTPServerConfig
	HTTPRouter *HTTPRouter
}

// Enforce Interface implementation
var _ IHTTPServer = (*HTTPServer)(nil)

// Listen Sets up and starts the HTTP server
func (s *HTTPServer) Listen() {

	// Set a defaut port if not set
	if s.Config.Port == 0 {
		s.Config.Port = 9080
	}

	// Check if no router defined, then create it
	if s.HTTPRouter == nil {
		s.initHTTPRouter()
	}

	// Start listening for HTTP connections
	portStr := fmt.Sprintf(":%d", s.Config.Port)
	log.Printf("HTTP HTTPServer listening on port %d\n", s.Config.Port)
	log.Fatal(http.ListenAndServe(portStr, s.HTTPRouter))
	log.Println("HTTP HTTPServer stopped")
}

// AddMiddleware Adds a middleware to HTTP HTTPServer (router in fact)
func (s *HTTPServer) AddMiddleware(name string, middleware HTTPMiddleware) {

	// Check if no router defined, then create it
	if s.HTTPRouter == nil {
		s.initHTTPRouter()
	}

	// Add the middleware
	s.HTTPRouter.AddMiddleware(name, middleware)
}

// AddStaticHandler adds a static handler to the router (for loading assets mainly)
func (s *HTTPServer) AddStaticHandler(name string, handler HTTPHandler) {
	s.HTTPRouter.AddStaticHandler(name, handler)
}

// AddRoute Adds a route to HTTP HTTPServer
func (s *HTTPServer) AddRoute(method string, path string, handler HTTPHandler, middlewares []string) {

	// Check if no router defined, then create it
	if s.HTTPRouter == nil {
		s.initHTTPRouter()
	}

	// Add the route
	s.HTTPRouter.AddRoute(method, path, handler, middlewares)
}

// initHTTPRouter creates a new HTTP HTTPRouter with default handler fallback
func (s *HTTPServer) initHTTPRouter() {

	// Set default handler for root (/)
	// TODO: work on this default one ... middlewares, etc?
	defaultHTTPHandler := func(w http.ResponseWriter, r *http.Request, params url.Values) {

		// So if OPTIONS or HEAD method, just 200
		// TODO: Check if this is ok???
		if r.Method == "OPTIONS" || r.Method == "HEAD" {
			w.WriteHeader(http.StatusOK)
			return
		}
		http.Error(w, "Not found", http.StatusNotFound)
	}

	// Create router with defatul handler
	s.HTTPRouter = NewHTTPRouter(defaultHTTPHandler)
}

// ISession HTTP Session interface
type ISession interface {
	GetID() string
	SetID(string)
	GetExpiresAt() time.Time
	SetExpiresAt(time.Time)
}

// Session structure
type Session struct {
	ID        string    `bson:"id" json:"id"`
	ExpiresAt time.Time `bson:"expires_at" json:"expires_at"`
}

// Enforce Interface implementation
var _ ISession = (*Session)(nil)

// GetID gets the session ID
func (s *Session) GetID() string {
	return s.ID
}

// SetID sets the session ID
func (s *Session) SetID(id string) {
	s.ID = id
}

// GetExpiresAt gets the session expiration date
func (s *Session) GetExpiresAt() time.Time {
	return s.ExpiresAt
}

// SetExpiresAt sets the session expiration date
func (s *Session) SetExpiresAt(t time.Time) {
	s.ExpiresAt = t
}

// IAuthSession HTTP Session interface
type IAuthSession interface {
	ISession
	GetAccountID() string
	SetAccountID(string)
}

// AuthSession structure
type AuthSession struct {
	Session
	AccountID string `bson:"account_id" json:"account_id"`
}

// Enforce Interface implementation
var _ IAuthSession = (*AuthSession)(nil)

// GetAccountID gets the session ID
func (s *AuthSession) GetAccountID() string {
	return s.AccountID
}

// SetAccountID sets the session ID
func (s *AuthSession) SetAccountID(id string) {
	s.AccountID = id
}

// SetSession creates a session into a cache layer
func SetSession(cacheClient ICacheClient, expiresIn time.Duration, accountID string) (ISession, error) {

	// No cache layer,  no session
	if cacheClient == nil {
		return nil, errors.New("Missing cache client for session")
	}

	// Init the session id and some other variables
	sessionID := ""
	maxAttempts := 20
	sessionExpires := 24 * time.Hour
	if expiresIn.Seconds() > 0 {
		sessionExpires = expiresIn
	}

	// Do a loop until it's saved into cache or exceeds max attempts
	for i := 1; i < maxAttempts; i++ {

		// Generate a random string
		sessionID = RandomString(32)

		// Try to get it
		exists, _ := cacheClient.Get("sessions:http:" + sessionID)

		// If no data returned, it doesn't exist, so no need to repeat this
		if exists == nil || exists == "" {
			break
		}

		// Otherwise we need to get another random string, let's reset it here just in case we exit by max attempts
		sessionID = ""
	}

	// Re-check if we got a proper id, otherwise fail .. miserabily .. mwahahhaa!!!
	if sessionID == "" {
		return nil, errors.New("Could not generate a proper session id")
	}

	// Now save that session id into cache layer
	baseSession := Session{
		ID:        sessionID,
		ExpiresAt: time.Now().Add(sessionExpires),
	}
	var session ISession
	if accountID != "" {
		session = &AuthSession{
			Session:   baseSession,
			AccountID: accountID,
		}
	} else {
		session = &baseSession
	}

	sessionBytes, err := json.Marshal(session)
	if err != nil {
		return nil, err
	}
	sessionStr := string(sessionBytes)
	err = cacheClient.Set("sessions:http:"+sessionID, sessionStr, sessionExpires)
	if err != nil {
		return nil, err
	}

	// Return successfully created session
	return session, nil
}

// GetSession gets a session by its id and given a cache client
func GetSession(id string, cacheClient ICacheClient) (ISession, error) {

	// No id no session
	if id == "" {
		return nil, errors.New("Missing id for session")
	}

	// No cache layer,  no session
	if cacheClient == nil {
		return nil, errors.New("Missing cache client for session")
	}

	// Get session from cache
	data, err := cacheClient.Get("sessions:http:" + id)
	if err != nil {
		return nil, err
	}
	if (data == nil) || (data == "") {
		return nil, errors.New("Session not found")
	}

	// Parse it into a session data
	var sessionData Session
	sessionStr, ok := data.(string)
	if !ok || (sessionStr == "") {
		return nil, errors.New("Could not parse session data")
	}
	sessionBytes := []byte(sessionStr)
	err = json.Unmarshal(sessionBytes, &sessionData)
	if err != nil {
		return nil, errors.New("Could not parse session data: " + err.Error())
	}

	// Return successful session
	return &sessionData, nil
}

// IHTTPController HTTP HTTPController interface
type IHTTPController interface {
	Handle(http.ResponseWriter, *http.Request, url.Values)
	GetCacheClient() ICacheClient
	GetComponent() IComponent
	HTTPJSONSuccess(http.ResponseWriter, interface{}, int)
	HTTPJSONError(http.ResponseWriter, string, int)
	HTTPJSONResponse(http.ResponseWriter, *Response)
	GetSession(*http.Request, string) (ISession, error)
	SetSession(http.ResponseWriter, string, time.Duration, string) (ISession, error)
	EnsureSession(http.ResponseWriter, *http.Request, string, time.Duration, string) (ISession, error)
}

// IHTTPFrontendController HTTP Frontend Controller interface
type IHTTPFrontendController interface {
	IHTTPController
	Preload()
}

// HTTPController HTTP HTTPController base struct
type HTTPController struct {
	CacheClient ICacheClient
	Component   IComponent
}

// HTTPFrontendController HTTP HTTPController base struct
type HTTPFrontendController struct {
	HTTPController
	Templates *template.Template
}

// GetCacheClient gets the underlying app
func (c *HTTPController) GetCacheClient() ICacheClient {
	return c.CacheClient
}

// GetComponent gets the underlying component, if any (as interface)
func (c *HTTPController) GetComponent() IComponent {
	return c.Component
}

// HTTPJSONSuccess wraps an HTTP success response into a JSON
func (c *HTTPController) HTTPJSONSuccess(w http.ResponseWriter, data interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	if data != nil && code != http.StatusNoContent {
		if _, err := json.Marshal(data); err != nil {
			c.HTTPJSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	w.WriteHeader(code)
	if data != nil && code != http.StatusNoContent {
		err := json.NewEncoder(w).Encode(data)
		if err != nil {
			log.Printf("Something went wrong outputting JSON to http: %v", data)
		}
	}
}

// HTTPJSONError wraps an HTTP error into a JSON
func (c *HTTPController) HTTPJSONError(w http.ResponseWriter, err string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	fmt.Fprintln(w, "{\"error\":\""+err+"\"}")
}

// HTTPJSONResponse wraps an HTTP response into a JSON
func (c *HTTPController) HTTPJSONResponse(w http.ResponseWriter, res *Response) {
	if res.Error != nil {
		c.HTTPJSONError(w, res.Error.Error(), res.Code)
		return
	}
	c.HTTPJSONSuccess(w, res.Data, res.Code)
}

// GetSession gets session by cookie
func (c *HTTPController) GetSession(r *http.Request, cookieName string) (ISession, error) {

	// Validate request
	if r == nil {
		return nil, errors.New("No request provided")
	}

	// Validate cookie name
	if cookieName == "" {
		return nil, errors.New("No cookie name provided")
	}

	// Get Cookie from http request
	cookie, err := r.Cookie(cookieName)
	if err != nil {
		return nil, err
	}

	// Get the session key/hash from Cookie
	sessionKey := cookie.Value

	// Check if expired
	if time.Now().After(cookie.Expires) {
		return nil, errors.New("Cookie is expired")
	}

	// Get session from package
	session, err := GetSession(sessionKey, c.CacheClient)
	if err != nil {
		return nil, err
	}
	if session == nil {
		return nil, errors.New("Session not found")
	}

	// Return successful session
	return session, nil
}

// SetSession sets session by cookie
func (c *HTTPController) SetSession(w http.ResponseWriter, cookieName string, expiresIn time.Duration, accountID string) (ISession, error) {

	// Validate response writer
	if w == nil {
		return nil, errors.New("No response writer provided")
	}

	// Validate cookie name
	if cookieName == "" {
		return nil, errors.New("No cookie name provided")
	}

	// Set default expires just in case
	sessionExpires := 24 * time.Hour
	if expiresIn.Seconds() > 0 {
		sessionExpires = expiresIn
	}

	// Try to set session
	session, err := SetSession(c.CacheClient, sessionExpires, accountID)
	if err != nil {
		return nil, err
	}
	if session == nil {
		return nil, errors.New("Session could not be set")
	}

	// Set Cookie into http response (no way to check it)
	cookie := http.Cookie{
		Name:     cookieName,
		Value:    session.GetID(),
		MaxAge:   int(sessionExpires.Seconds()),
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)

	// Return successful session
	return session, nil
}

// EnsureSession ensures session by cookie exists, creating it otherwise
func (c *HTTPController) EnsureSession(w http.ResponseWriter, r *http.Request, cookieName string, expiresIn time.Duration, accountID string) (ISession, error) {

	// Try to get session first
	session, err := c.GetSession(r, cookieName)

	// If no session, try to set it
	if (err != nil) || (session == nil) {
		session, err = c.SetSession(w, cookieName, expiresIn, accountID)
		if err != nil {
			return nil, err
		}
	}

	// At this poit, return succesful session
	return session, nil
}

// HTTPEndpoint defines a single endpoint as its URL and needed Headers
type HTTPEndpoint struct {
	URL     string
	Headers map[string][]string
}

// HTTPEndpoints define how to interface with external services (APIs) thru HTTP (CRUD/RESTful)
type HTTPEndpoints struct {
	Base   HTTPEndpoint
	Get    HTTPEndpoint
	Post   HTTPEndpoint
	Put    HTTPEndpoint
	Delete HTTPEndpoint
}

// HTTPBaseResource defines the base resource structure (mostly an id)
type HTTPBaseResource struct {
	ID interface{} `json:"id"`
}

// IHTTPRepository Global http (API based) repository interface
type IHTTPRepository interface {
	IRepository
	Init()
	SetHTTPClient(*http.Client)
	FindModels(interface{}, interface{}, int, ...string) (interface{}, error)
	FindModel(interface{}, interface{}, ...string) (interface{}, error)
	GetModel(interface{}, interface{}) (interface{}, error)
}

// HTTPRepository Global http repository struct
type HTTPRepository struct {
	Repository
	Endpoints HTTPEndpoints
	Client    *http.Client
}

// Enforce Interface implementation
var _ IHTTPRepository = (*HTTPRepository)(nil)

// Fancy regexp for overriding endpoints
var httpEndpointOverrideRegexp = regexp.MustCompile(`^https?:\/\/.+$`)

// Init initializes repository
func (r *HTTPRepository) Init() {
	if r.Client == nil {
		r.Client = &http.Client{}
	}
}

// SetHTTPClient sets repository HTTP client
func (r *HTTPRepository) SetHTTPClient(client *http.Client) {
	r.Client = client
}

// FindModels finds resources, it returns/fills a pointer to a collection of provided interfaces
func (r *HTTPRepository) FindModels(models interface{}, query interface{}, limit int, sort ...string) (interface{}, error) {

	// Build endpoint URL, fail if not defined after all
	endpointURL := r.BuildEndpointURL(r.Endpoints.Get)
	if endpointURL == "" {
		return nil, errors.New("No endpoint URL defined for GET")
	}

	// Try to parse incoming query string, adding it if not empty
	queryStr := ""
	if query != nil {

		// Try first to cast it to a map
		qMap, ok := query.(map[string]interface{})
		if ok {

			// Map to query string
			for k, v := range qMap {
				queryStr = fmt.Sprintf("%s%s=%v&", queryStr, k, v)
			}
			queryStr = strings.TrimRight(queryStr, "&")
		} else {

			// Now try to cast to a query string
			qStr, ok := query.(string)
			if !ok {
				return nil, errors.New("Could not cast query interface to either map or string")
			}
			queryStr = queryStr + qStr
		}
	}

	// Try to parse limit and fill in query
	if limit > 0 {
		if len(queryStr) > 0 {
			queryStr = queryStr + "&"
		}
		queryStr = fmt.Sprintf(queryStr+"limit=%d", limit)
	}

	// Try to parse sort and fill in query
	if len(sort) > 0 {
		if len(queryStr) > 0 {
			queryStr = queryStr + "&"
		}
		queryStr = queryStr + "sort="
		for _, sorting := range sort {
			queryStr = fmt.Sprintf(queryStr+"%s,", sorting)
		}
		queryStr = strings.TrimRight(queryStr, ",")
	}

	// Final touch
	if len(queryStr) > 0 {
		queryStr = "?" + queryStr
	}

	// Form url
	url := endpointURL + queryStr

	// Create http request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// Add headers, if any
	r.MergeHeaders(req, r.Endpoints.Base)
	r.MergeHeaders(req, r.Endpoints.Get)

	// Execute request, fail on error
	res, err := r.Client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, errors.New(res.Status)
	}

	// Defer body close, read it
	defer res.Body.Close()
	byteData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("Error reading response bytecode. %s", err.Error())
	}

	// Parse to array of interface models,
	err = json.Unmarshal(byteData, models)
	if err != nil {
		return nil, fmt.Errorf("Error parsing response bytecode to interface collection. %s", err.Error())
	}

	// Return result
	return models, nil
}

// Find resources, it returns a pointer to a collection of generic interfaces
func (r *HTTPRepository) Find(query interface{}, limit int, sort ...string) (interface{}, error) {

	// Create collection of dummy models
	models := []interface{}{}

	// Delegate into the other method
	return r.FindModels(&models, query, limit, sort...)
}

// FindModel finds a single resource, it returns/fills provided interface (pointer)
func (r *HTTPRepository) FindModel(model interface{}, query interface{}, sort ...string) (interface{}, error) {

	// Delegate into general Find method
	data, err := r.Find(query, 1, sort...)
	if err != nil {
		return nil, err
	}

	// Cast data into generic interface collection or fail
	entries, ok := data.(*[]interface{})
	if !ok {
		return nil, errors.New("Could not cast received data into generic interface collection")
	}

	// Ensure not empty and get first one
	if len(*entries) < 1 {
		return nil, errors.New("404 Not Found")
	}

	// Marshall/Unmarshall into model
	byteData, err := json.Marshal((*entries)[0])
	if err != nil {
		return nil, fmt.Errorf("Could not serialize received generic interface. %s", err.Error())
	}
	err = json.Unmarshal(byteData, model)
	if err != nil {
		return nil, fmt.Errorf("Could not cast queried resource to provided model. %s", err.Error())
	}

	// Return it
	return model, nil
}

// FindOne single resource, it returns a single generic interface
func (r *HTTPRepository) FindOne(query interface{}, sort ...string) (interface{}, error) {

	// Create dummy model
	var model interface{}

	// Delegate into the other method
	return r.FindModel(&model, query, sort...)
}

// GetModel gets a single resource by id, it fills/returns a provided interface (pointer)
func (r *HTTPRepository) GetModel(model interface{}, id interface{}) (interface{}, error) {

	// Build endpoint URL, fail if not defined after all
	endpointURL := r.BuildEndpointURL(r.Endpoints.Get)
	if endpointURL == "" {
		return nil, errors.New("No endpoint URL defined for GET")
	}

	// Form url or fail
	idStr, err := r.IDToString(id)
	if err != nil {
		return nil, err
	}
	url := endpointURL + "/" + idStr

	// Create http request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// Add headers, if any
	r.MergeHeaders(req, r.Endpoints.Base)
	r.MergeHeaders(req, r.Endpoints.Get)

	// Execute request, fail on error or whether not a 200 OK
	res, err := r.Client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, errors.New(res.Status)
	}

	// Defer body close, read it
	defer res.Body.Close()
	byteData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("Error reading response bytecode. %s", err.Error())
	}

	// Parse to array of dummy entries,
	err = json.Unmarshal(byteData, model)
	if err != nil {
		return nil, fmt.Errorf("Error parsing response bytecode to generic interface. %s", err.Error())
	}

	// Return result
	return model, nil
}

// Get single resource by id, it returns a generic interface
func (r *HTTPRepository) Get(id interface{}) (interface{}, error) {

	// Create dummy model
	var model interface{}

	// Delegate into the other method
	return r.GetModel(&model, id)
}

// Create a resource
func (r *HTTPRepository) Create(in interface{}) error {

	// Build endpoint URL, fail if not defined after all
	endpointURL := r.BuildEndpointURL(r.Endpoints.Post)
	if endpointURL == "" {
		return errors.New("No endpoint URL defined for POST")
	}

	// Try to serialize input
	resourceBytes, err := json.Marshal(in)
	if err != nil {
		return errors.New("Could not serialize JSON object")
	}

	// Cast input to proper io.Reader or die
	bodyReader := bytes.NewReader(resourceBytes)
	if bodyReader == nil {
		return errors.New("Could not create reader from bytecode")
	}

	// Form url
	url := endpointURL

	// Create http request
	req, err := http.NewRequest("POST", url, bodyReader)
	if err != nil {
		return err
	}

	// Add headers, if any
	r.MergeHeaders(req, r.Endpoints.Base)
	r.MergeHeaders(req, r.Endpoints.Post)

	// Execute request, fail on error or not 2xx expected status code
	res, err := r.Client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusCreated && res.StatusCode != http.StatusOK && res.StatusCode != http.StatusNoContent {
		return errors.New(res.Status)
	}

	// Return no errors
	return nil
}

// Update a resource
func (r *HTTPRepository) Update(fields interface{}, in interface{}) error {

	// First delegate into Find method
	data, err := r.FindOne(fields)
	if err != nil {
		return err
	}

	// Marshall/Unmarshall into base resource
	byteData, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("Could not serialize received generic interface. %s", err.Error())
	}
	var baseRsc HTTPBaseResource
	err = json.Unmarshal(byteData, &baseRsc)
	if err != nil {
		return fmt.Errorf("Could not cast queried resource to an id-entifiable entity. %s", err.Error())
	}

	// Now delegate into the UpdateID method
	return r.UpdateID(baseRsc.ID, in)
}

// UpdateID a resource by its id
func (r *HTTPRepository) UpdateID(id interface{}, in interface{}) error {

	// Build endpoint URL, fail if not defined after all
	endpointURL := r.BuildEndpointURL(r.Endpoints.Put)
	if endpointURL == "" {
		return errors.New("No endpoint URL defined for PUT")
	}

	// Form url or fail
	idStr, err := r.IDToString(id)
	if err != nil {
		return err
	}
	url := endpointURL + "/" + idStr

	// Try to serialize it
	resourceBytes, err := json.Marshal(in)
	if err != nil {
		return errors.New("Could not serialize JSON object")
	}

	// Cast input to proper io.Reader or die
	bodyReader := bytes.NewReader(resourceBytes)
	if bodyReader == nil {
		return errors.New("Could not create reader from bytecode")
	}

	// Create http request
	req, err := http.NewRequest("PUT", url, bodyReader)
	if err != nil {
		return err
	}

	// Add headers, if any
	r.MergeHeaders(req, r.Endpoints.Base)
	r.MergeHeaders(req, r.Endpoints.Put)

	// Execute request, fail on error or status code different than 200/204
	res, err := r.Client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusNoContent {
		return errors.New(res.Status)
	}

	// Return no errors
	return nil
}

// Delete single resource by id
func (r *HTTPRepository) Delete(id interface{}) error {

	// Build endpoint URL, fail if not defined after all
	endpointURL := r.BuildEndpointURL(r.Endpoints.Delete)
	if endpointURL == "" {
		return errors.New("No endpoint URL defined for DELETE")
	}

	// Form url or fail
	idStr, err := r.IDToString(id)
	if err != nil {
		return err
	}
	url := endpointURL + "/" + idStr

	// Create http request
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	// Add headers, if any
	r.MergeHeaders(req, r.Endpoints.Base)
	r.MergeHeaders(req, r.Endpoints.Delete)

	// Execute request, fail on error or status code different than 200/204
	res, err := r.Client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK && res.StatusCode != http.StatusNoContent {
		return errors.New(res.Status)
	}

	// Return no errors
	return nil
}

// MergeHeaders utility
func (r *HTTPRepository) MergeHeaders(req *http.Request, endpoint HTTPEndpoint) (*http.Request, error) {

	// Loop over endpoint headers
	for k, v := range endpoint.Headers {

		// Loop over values and append to final headers
		for _, j := range v {
			req.Header.Add(k, j)
		}
	}

	// Return modified request
	return req, nil
}

// BuildEndpointURL utility
func (r *HTTPRepository) BuildEndpointURL(endpoint HTTPEndpoint) string {

	// Start url from base endpoint
	endpointURL := r.Endpoints.Base.URL

	// Append/Replace with GET endpoint if defined
	methodEndpointURL := endpoint.URL
	if methodEndpointURL != "" {

		// If the particular endpoint seems to be an absolute one, replace it, otherwise append it
		if httpEndpointOverrideRegexp.MatchString(methodEndpointURL) {
			endpointURL = methodEndpointURL
		} else {
			endpointURL += methodEndpointURL
		}
	}

	// Return it
	return endpointURL
}

// IDToString utility, we only support string or number in such interface
func (r *HTTPRepository) IDToString(in interface{}) (string, error) {

	// Ensure not empty
	if in == nil || in == "" || in == 0 {
		return "", errors.New("No ID provided")
	}

	// Try to cast to string directly or seek other options
	idStr, ok := in.(string)
	if !ok {

		// Try with integer or fail
		idInt, ok := in.(int)
		if !ok {
			return "", errors.New("ID is not string or integer")
		}

		// Convert integer to string
		idStr = strconv.Itoa(idInt)
	}

	// If still empty, fail
	if idStr == "" {
		return "", errors.New("ID is empty")
	}

	// Return id string
	return idStr, nil
}

// HTTPModelRequest is a helper that wraps a generic request
func HTTPModelRequest(method string, url string, body interface{}, headers http.Header, model interface{}) (interface{}, int, error) {

	// Create HTTP client
	client := &http.Client{}

	// Declare empty buffer
	var buf bytes.Buffer

	// Write body into buffer if provided
	if body != nil {

		// Get JSON bytecode
		jsonBytes, err := json.Marshal(body)
		if err != nil {
			return nil, http.StatusBadRequest, err
		}

		// Write bytecode into buffer
		_, err = buf.Write(jsonBytes)
		if err != nil {
			return nil, http.StatusBadRequest, err
		}
	}

	// Create HTTP request or fail, assign context
	req, err := http.NewRequest(method, url, &buf)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	// Set relevant headers into request (direct copy)
	req.Header = headers

	// Perform HTTP request or fail, defer body close
	res, err := client.Do(req)
	if err != nil {
		return nil, res.StatusCode, err
	}
	defer res.Body.Close()

	// Read bytecode from body reader
	byteData, err2 := ioutil.ReadAll(res.Body)
	if err2 != nil {
		return nil, http.StatusInternalServerError, fmt.Errorf("Error reading response bytecode: %s", err2.Error())
	}

	// Parse to provided model if data
	if len(byteData) > 0 {

		// If model is not present, try to get generic interface
		var dummy interface{}
		if model == nil {
			model = &dummy
		}

		// Try to parse into the particular passed model
		err2 = json.Unmarshal(byteData, model)

		// Error parsing
		if err2 != nil {
			return nil, http.StatusInternalServerError, err2
		}

		// Model was not filled
		if model == nil {
			return nil, http.StatusInternalServerError, errors.New("No model filled")
		}

		// Also, try to parse body to generic error so we add it to error, actually
		genericError := struct {
			Error string `json:"error"`
		}{}
		err2 = json.Unmarshal(byteData, &genericError)
		if err2 == nil && genericError.Error != "" {
			return model, res.StatusCode, errors.New(genericError.Error)
		}
	}

	// All good, return data with response status and error if any
	return model, res.StatusCode, err
}

// GetMockHTTPClient gets a mock HTTP client, using a testing server, it returns also a function for closing it
func GetMockHTTPClient(handler http.Handler) (*http.Client, func()) {
	s := httptest.NewServer(handler)
	cli := &http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
				return net.Dial(network, s.Listener.Addr().String())
			},
		},
	}
	return cli, s.Close
}
