package gobase

import (
	"io/ioutil"
	"log"
)

// Environment Global Environment structure, full service config
type Environment struct {
	Version            string              `json:"version"`
	MongoDatabase      *MongoDBConfig      `json:"mongo_database"`
	PostgreSQLDatabase *PostgreSQLDBConfig `json:"postgresql_database"`
	RedisCache         *RedisConfig        `json:"redis_cache"`
	RabbitMQ           *RabbitMQConfig     `json:"rabbitmq"`
	HTTPServer         *HTTPServerConfig   `json:"http_server"`
	Logger             *LoggerConfig       `json:"logger"`
}

// ReadFromFile reads environment from a JSON file
func (e *Environment) ReadFromFile(path string) error {

	// Get environment configuration from file system (JSON)
	log.Println("Reading environment configuration from " + path + " ...")
	cfgRaw, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	// Parse configuration
	log.Println("Environment configuration found, parsing it ...")
	err = FromJSON(e, cfgRaw)
	if err != nil {
		return err
	}
	log.Println("Environment configuration succesfully parsed")

	// Return no error
	return nil
}
