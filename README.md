# gokit

Small toolkit library for using into component-based services

# Import into workspace

`go get gitlab.com/frontcoder/gokit`

# Run tests

`go test`   in root folder

## Features

It contains some useful abstractions:

- Database connection, client and repository interfaces: MongoDB, PosgreSQL.
- HTTP server, client and repository interfaces.
- Websocket server, client and message protocol interfaces.
- Redis cache client interface.
- RabbitMQ asynchronous messaging queue client and message protocol interface.
- Various utilities, including Json Web Token encoder/decoder.

It also implements structures for organizing larger apps/services

Top level Application containing:

- Servers: HTTP, Websockets
- Clients: Databases (SQL and NoSQL), Cache, AMQP Producer/Consumer
- Components collection and Services bus for internal communication between them
- Others: Environment, Configuration, Logger, etc.  

Component-based architecture, definining each one with:

- Repositories: SQL, NoSQL, HTTP
- Interfaces/Drivers: HTTP, Websockets (AMQP pending)
- Services: The business logic, communicating thru internal BUS or external Interfaces/Drivers

## Reason for this to exist

As a result of repeating code for several projects this toolkit was born.
It helped to reduce the amount of writing and to learn further this particular language.

## Pending to do in the future

- Implement remaining tests
- Integrate AMQP as Component Interface/Driver
- Add more Database implementations?
