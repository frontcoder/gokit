module gitlab.com/frontcoder/gokit

go 1.13

require (
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/lib/pq v1.3.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
