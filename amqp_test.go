package gobase

import (
	"errors"
	"github.com/streadway/amqp"
	"log"
	"strings"
	"testing"
	"time"
)

// Global variables for theses tests
var rabbitmqConfig *RabbitMQConfig
var rabbitmqClient IAMQClient
var rabbitmqConsumer IAMQConsumer
var rabbitmqProducer IAMQProducer

// loadAMQPEnv reads environment for these tests from globally parsed environment file
func loadAMQPEnv() (*RabbitMQConfig, error) {

	// Parse configuration
	config := TestEnv.RabbitMQ
	if config == nil {
		return nil, errors.New("Missing RabbitMQ environment configuration")
	}

	// Fail if no consumer queues defined
	if len(config.ConsumerQueues) < 1 {
		return nil, errors.New("No consumer queues in environment configuration")
	}

	// Return no error
	return config, nil
}

// TestAMQP runs actually the tests
func TestAMQP(t *testing.T) {

	// Load environment configuration or fail
	var err error
	rabbitmqConfig, err = loadAMQPEnv()
	if err != nil {
		t.Error(err.Error())
		return
	}

	// Create client with such configuration
	rabbitmqClient = &RabbitMQClient{Config: *rabbitmqConfig}

	// Run tests against client, eith needed sleep times in between
	if b := t.Run("connect", testAMQPConnect); !b {
		t.Error(errors.New("Could not connect client"))
		return
	}
	time.Sleep(2 * time.Second)
	t.Run("publish", testAMQPPublish)
	time.Sleep(2 * time.Second)
	t.Run("disconnect", testAMQPDisconnect)
}

// testAMQPClientConnect tests proper client inizialization/connection (producer & consumer)
func testAMQPClientConnect(t *testing.T, c IAMQClient) {
	var err error
	rabbitmqConsumer, rabbitmqProducer, err = c.Init()
	if err != nil {
		t.Error(err.Error())
	}
	if rabbitmqProducer == nil {
		t.Error(errors.New("AMQP Producer not properly instantiated"))
	}
	if rabbitmqConsumer == nil {
		t.Error(errors.New("AMQP Consumer not properly instantiated"))
	}
}

// testAMQPConnect tests amqp connection
func testAMQPConnect(t *testing.T) {
	testAMQPClientConnect(t, rabbitmqClient)
}

// testAMQPConsumerDisconnect tests consumer disconnection
func testAMQPConsumerDisconnect(t *testing.T, c IAMQConsumer) {
	if err := rabbitmqConsumer.GetConnection().Close(); err != nil {
		t.Error(err.Error())
	}
}

// testAMQPProducerDisconnect tests producer disconnection
func testAMQPProducerDisconnect(t *testing.T, c IAMQProducer) {
	if err := rabbitmqProducer.GetConnection().Close(); err != nil {
		t.Error(err.Error())
	}
}

// testAMQPDisconnect tests amqp total disconnection
func testAMQPDisconnect(t *testing.T) {
	testAMQPConsumerDisconnect(t, rabbitmqConsumer)
	testAMQPProducerDisconnect(t, rabbitmqProducer)
}

// testAMQPPublish tests a message is properly published/consumed to/from the queue
func testAMQPPublish(t *testing.T) {

	// Create a channel to listen for possible errors
	errChan := make(chan string)

	// Configure rabbitmq channel
	ch, ok := rabbitmqProducer.GetChannel().GetChannel().(*amqp.Channel)
	if !ok {
		t.Error(errors.New("Could not cast rabbitmq channel"))
	}
	ch.Confirm(false)

	// Create channel to listen for confirmations
	cChan := make(chan amqp.Confirmation)
	cChan = ch.NotifyPublish(cChan)
	if cChan == nil {
		t.Error(errors.New("Could not properly set confirmation notify chan"))
	}

	// Run goroutine getting the confirmation events into channel, or sending errors otherwise
	go func() {
		for msg := range cChan {
			log.Printf("Received confirm: %v", msg)
			if !msg.Ack {
				errChan <- "Message not acked in destination"
			} else {
				errChan <- ""
			}
		}
	}()

	// Create channel to listen for notifications (errors mainly?)
	rChan := make(chan amqp.Return)
	rChan = ch.NotifyReturn(rChan)
	if rChan == nil {
		t.Error(errors.New("Could not properly return notify chan"))
	}

	// Run goroutine to listen for notifications, so we show the relevant error if any
	go func() {
		for msg := range rChan {
			log.Printf("Received return: %v", msg)
			errChan <- "Message returned from destination"
		}
	}()

	// Create a message to send
	msg := AMQMessage{
		Persistent: true,
		Data: &AMQMessageData{
			Text: "yo!!",
		},
	}

	// Pick up first consuming queue (testing one should be enough), and get topic
	queue := rabbitmqConfig.ConsumerQueues[0]
	topic := rabbitmqConfig.QueuesConfig[queue].Topic
	if topic == "" {
		t.Error(errors.New("No proper topic for consumer queue"))
	}

	// Now let'a replace any wildcards (#) with some random work, like 'some'
	newTopic := strings.ReplaceAll(topic, "#", "some")

	// Publish the message
	err := rabbitmqProducer.Publish(rabbitmqConfig.DefaultExchange, newTopic, &msg)
	if err != nil {
		t.Error(err.Error())
	}

	// Wait for feedback, it should be empty error to succeed
	if errTxt := <-errChan; errTxt != "" {
		t.Error(errTxt)
	}
}
