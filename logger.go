package gobase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

// DefaultLogLevel set the default log level
var DefaultLogLevel = "warn"

// LogLevelsMap maps levels to integers
var LogLevelsMap = map[string]int{
	"debug": 0,
	"info":  1,
	"warn":  2,
	"error": 3,
}

// ILogger interface
type ILogger interface {
	GetConfig() LoggerConfig
	GetLevel() string
	SetLevel(string)
	SetFlags(int)
	SetOutput(io.Writer)
	Log(string, string, ...interface{})
	Debug(string, ...interface{})
	Info(string, ...interface{})
	Warn(string, ...interface{})
	Error(string, ...interface{})
}

// LoggerConfig model for logging
type LoggerConfig struct {
	Level          string       `json:"level"`
	ShowTimestamp  bool         `json:"show_timestamp"`
	ShowSourceFile bool         `json:"show_source_file"`
	Slack          *SlackConfig `json:"slack"`
}

// Logger main struct
type Logger struct {
	log.Logger
	Config    LoggerConfig `json:"config"`
	flagsSet  bool
	outputSet bool
}

// Enforce Interface implementation
var _ ILogger = (*Logger)(nil)

// check if log should proceed by its level
func (l *Logger) check(level string) bool {

	// Get app level and its index, also assign default if needed
	currLevel := l.GetLevel()
	if currLevel == "" {
		currLevel = DefaultLogLevel
	}
	l.SetLevel(currLevel)
	currLevelIdx, ok := LogLevelsMap[currLevel]
	if !ok {
		currLevelIdx = LogLevelsMap["warn"]
	}

	// Get desired level and its index
	if level == "" {
		level = DefaultLogLevel
	}
	levelIdx, ok := LogLevelsMap[level]
	if !ok {
		levelIdx = LogLevelsMap["warn"]
	}

	// Return if enough level configured
	return currLevelIdx <= levelIdx
}

// GetConfig gets current log configuration
func (l *Logger) GetConfig() LoggerConfig {
	return l.Config
}

// GetLevel gets current log level
func (l *Logger) GetLevel() string {
	return l.Config.Level
}

// SetLevel sets current log level
func (l *Logger) SetLevel(lvl string) {
	l.Config.Level = lvl
}

// SetFlags sets current tags
// @override
func (l *Logger) SetFlags(flags int) {
	l.Logger.SetFlags(flags)
	l.flagsSet = true
}

// SetOutput sets current output
// @override
func (l *Logger) SetOutput(out io.Writer) {
	l.Logger.SetOutput(out)
	l.outputSet = true
}

// Log a string, given a level and with optional formatting parameters
func (l *Logger) Log(level string, txt string, params ...interface{}) {

	// Check if enough level configured, otherwise skip
	if !l.check(level) {
		return
	}

	// Skip if no text
	if txt == "" {
		return
	}

	// Check if flags were not set yet
	if !l.flagsSet {

		// Get them from config and start adding
		flags := 0
		if l.Config.ShowTimestamp {
			flags = flags | log.Ldate | log.Ltime | log.Lmicroseconds | log.LUTC
		}
		if l.Config.ShowSourceFile {
			flags = flags | log.Llongfile
		}

		// Set them
		l.SetFlags(flags)
	}

	// Log it!
	l.Printf("["+level+"] "+txt, params...)
	// log.Printf("%v", l)
}

// Debug wrapper for debug level logging
func (l *Logger) Debug(txt string, params ...interface{}) {
	if !l.outputSet {
		l.SetOutput(os.Stdout)
	}
	l.Log("debug", txt, params...)
}

// Info wrapper for info level logging
func (l *Logger) Info(txt string, params ...interface{}) {
	if !l.outputSet {
		l.SetOutput(os.Stdout)
	}
	l.Log("info", txt, params...)
}

// Warn wrapper for warn level logging
func (l *Logger) Warn(txt string, params ...interface{}) {
	if !l.outputSet {
		l.SetOutput(os.Stdout)
	}
	l.Log("warn", txt, params...)
}

// Error wrapper for error level logging
func (l *Logger) Error(txt string, params ...interface{}) {
	if !l.outputSet {
		l.SetOutput(os.Stderr)
	}
	l.Log("error", txt, params...)
}

// SlackPayload defines what to send
type SlackPayload struct {
	Text      string  `json:"text"`
	Username  *string `json:"username,omitempty"`
	IconEmoji *string `json:"icon_emoji,omitempty"`
	Channel   *string `json:"channel,omitempty"`
}

// ISlackLogger interface
type ISlackLogger interface {
	ILogger
	Send(string, string, ...string) error
	SendDebug(string, ...string) error
	SendInfo(string, ...string) error
	SendWarn(string, ...string) error
	SendError(string, ...string) error
}

// SlackConfig model for logging
type SlackConfig struct {
	Endpoint string `json:"endpoint"`
}

// SlackLogger main struct
type SlackLogger struct {
	Logger
}

// Enforce Interface implementation
var _ ISlackLogger = (*SlackLogger)(nil)

// Send logs a string into a slack channel, optionally overriding channel, user and icon
// Order of args is: channel, username, icon_emoji
// IMPORTANT! No placeholders here, you should fill them into text rith fmt.Sprintf beforehand
// Also no flags here!
func (l *SlackLogger) Send(level string, txt string, args ...string) error {

	// Get endpoint url or fail
	slackConfig := l.GetConfig().Slack
	if slackConfig == nil {
		return fmt.Errorf("There's no slack configuration for logger")
	}
	url := slackConfig.Endpoint
	if url == "" {
		return fmt.Errorf("There's no endpoint configured for slack")
	}

	// Skip if not enough level
	if !l.check(level) {
		l.Warn("Not enough level. Configured: %s, Desired: %s", l.Config.Level, level)
		return nil
	}

	// Skip if no text
	if txt == "" {
		l.Warn("No content to send")
		return nil
	}

	// Create minimum payload
	payload := SlackPayload{Text: txt}

	// Set channel if not empty
	if (len(args) > 0) && (args[0] != "") {
		payload.Channel = &args[0]
	}

	// Set user if not empty
	if (len(args) > 1) && (args[1] != "") {
		payload.Username = &args[1]
	}

	// Set icon if not empty
	if (len(args) > 2) && (args[2] != "") {
		payload.IconEmoji = &args[2]
	}

	// Marshall payload into JSON
	body, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("Could not parse slack payload:\n%s", txt)
	}

	// Send it thru http
	res, err := http.Post(url, "application/json", bytes.NewReader(body))
	if err != nil {
		return fmt.Errorf("Could not send message to slack:\nError: %s\nSent: %s", err.Error(), txt)
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("Sent message to slack returned bad status code: %d\nSent: %s", res.StatusCode, txt)
	}

	// All good
	return nil
}

// SendDebug wrapper around debug level slack log
func (l *SlackLogger) SendDebug(txt string, args ...string) error {
	return l.Send("debug", txt, args...)
}

// SendInfo wrapper around info level slack log
func (l *SlackLogger) SendInfo(txt string, args ...string) error {
	return l.Send("debug", txt, args...)
}

// SendWarn wrapper around warning level slack log
func (l *SlackLogger) SendWarn(txt string, args ...string) error {
	return l.Send("debug", txt, args...)
}

// SendError wrapper around error level slack log
func (l *SlackLogger) SendError(txt string, args ...string) error {
	return l.Send("debug", txt, args...)
}
