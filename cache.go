package gobase

import (
	"errors"
	"github.com/go-redis/redis"
	"log"
	"time"
)

// ICacheClient interface
type ICacheClient interface {
	Connect() (interface{}, error)
	Disconnect() error
	GetConnection() interface{}
	Get(key string) (interface{}, error)
	Set(key string, val interface{}, exp time.Duration) error
	Del(key string) error
	Keys(pattern string) ([]string, error)
	PingConnection() error
}

// RedisConfig Redis config model
type RedisConfig struct {
	MasterName string   `json:"master_name"`
	Hosts      []string `json:"hosts"`
	Database   int      `json:"database"`
	Password   string   `json:"password"`
}

// RedisClient Global struct
type RedisClient struct {
	Config     RedisConfig
	Connection redis.UniversalClient
}

// Enforce Interface implementation
var _ ICacheClient = (*RedisClient)(nil)

// Connect Connects and returns a Redis connection
func (c *RedisClient) Connect() (interface{}, error) {

	// We'll use this flag to know whether we use standalone server or failover ones
	useFailover := true

	// Get hosts configuration or use defaults (localhost single instance)
	hostsArr := c.Config.Hosts
	if len(hostsArr) < 1 {
		hostsArr = []string{":6379"}
		useFailover = false
	}

	// Get master name, update failover flag
	masterName := c.Config.MasterName
	useFailover = useFailover && masterName != ""

	// Start configuration
	redisOptions := redis.UniversalOptions{
		MasterName:         masterName,
		Addrs:              hostsArr,
		Password:           c.Config.Password,
		DB:                 c.Config.Database,
		MaxRetries:         3,
		MinRetryBackoff:    200000,
		MaxRetryBackoff:    1000000,
		IdleTimeout:        -1,
		IdleCheckFrequency: -1,
	}

	// Add master name if failover configured
	if useFailover {
		redisOptions.MasterName = masterName
	}

	// Connect the client on failover or standalone fashion
	log.Println("Connecting to Redis server")
	c.Connection = redis.NewUniversalClient(&redisOptions)

	// if useFailover {
	// 	c.Connection = redis.NewFailoverClient(&redis.FailoverOptions{
	// 		MasterName:         masterName,
	// 		SentinelAddrs:      hostsArr,
	// 		Password:           c.Config.Password,
	// 		DB:                 c.Config.Database,
	// 		MaxRetries:         3,
	// 		MinRetryBackoff:    200000,
	// 		MaxRetryBackoff:    1000000,
	// 		IdleTimeout:        -1,
	// 		IdleCheckFrequency: -1,
	// 	})
	// } else {
	// 	c.Connection = redis.NewClient(&redis.Options{
	// 		Addr:               hostsArr[0],
	// 		Password:           c.Config.Password,
	// 		DB:                 c.Config.Database,
	// 		MaxRetries:         3,
	// 		MinRetryBackoff:    200000,
	// 		MaxRetryBackoff:    1000000,
	// 		IdleTimeout:        -1,
	// 		IdleCheckFrequency: -1,
	// 	})
	// }

	// Fail if no connection retrieved, or ping fails
	if c.Connection == nil {
		return nil, errors.New("Could not connect Redis client")
	}
	if err := c.Connection.Ping().Err(); err != nil {
		return nil, err
	}
	log.Println("Redis connected")

	// Return no errors
	return c.Connection, nil
}

// Disconnect Closes and releases connection
func (c *RedisClient) Disconnect() error {
	return c.Connection.Close()
}

// GetConnection Gets inner connection
func (c *RedisClient) GetConnection() interface{} {
	return c.Connection
}

// PingConnection makes a ping to server
func (c *RedisClient) PingConnection() error {
	return c.Connection.Ping().Err()
}

// Get Gets a key value
func (c *RedisClient) Get(key string) (interface{}, error) {
	return c.Connection.Get(key).Result()
}

// Set Sets a key/value with expiration (in nanoseconds!)
func (c *RedisClient) Set(key string, val interface{}, exp time.Duration) error {
	return c.Connection.Set(key, val, exp).Err()
}

// Del Deletes a key
func (c *RedisClient) Del(key string) error {
	return c.Connection.Del([]string{key}...).Err()
}

// Keys all keys matching a pattern
func (c *RedisClient) Keys(pattern string) ([]string, error) {
	return c.Connection.Keys(pattern).Result()
}
