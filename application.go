/*
Package gobase - Core package.

Contains definitions for the foundation of the 'framework'.
*/
package gobase

import (
	"context"
	"fmt"
	"net/http"
)

// ApplicationHealthCheckPayload is the health check response data
// TODO: Add more data as there's need for it
type ApplicationHealthCheckPayload struct {
	DBClient    string `json:"db_client"`
	CacheClient string `json:"cache_client"`
}

// Application global core services wrapper, to pass to components
type Application struct {
	Environment       Environment
	Config            interface{}
	Logger            ISlackLogger
	DBClient          IDBClient
	RDBClient         IDBClient
	CacheClient       ICacheClient
	AMQConsumer       IAMQConsumer
	AMQProducer       IAMQProducer
	HTTPServer        IHTTPServer
	WSServer          IWSServer
	ServicesChannel   chan *ServiceMessage
	Context           context.Context
	ContextCancelFunc context.CancelFunc
	BasePath          string
	Components        map[string]IComponent
}

// ChannelCall Forwards a service call to the events handler, waits response into provided channel
func (a *Application) ChannelCall(service string, request interface{}, ch chan *Response) {
	msg := ServiceMessage{
		RequestTarget:   service,
		RequestData:     request,
		ResponseChannel: ch,
	}
	a.ServicesChannel <- &msg
}

// Call Forwards a service call to the events handler, waiting into temporary channel and returning the response
func (a *Application) Call(service string, request interface{}) *Response {
	ch := make(chan *Response)
	a.ChannelCall(service, request, ch)
	res := <-ch
	return res
}

// AsyncCall Forwards a service call to the events handler, waiting into temporary channel and processing the response
func (a *Application) AsyncCall(service string, request interface{}, fn func(*Response)) {
	fn(a.Call(service, request))
}

// HealthCheck checks minimum requirements for the app to be working
// TODO: Grow it with more data as per demand (so far db + cache only, no rdb, amq, etc.)
func (a *Application) HealthCheck() *Response {

	// Initialize data
	hcData := ApplicationHealthCheckPayload{}
	allFine := true

	// Check if DB CLient, and if any, ping the server connection
	if a.DBClient != nil {
		if err := a.DBClient.PingConnection(); err != nil {
			hcData.DBClient = fmt.Sprintf("Failure: %s", err.Error())
			allFine = false
		} else {
			hcData.DBClient = "OK"
		}
	}

	// Check if Cache CLient, and if any, ping the server connection
	if a.CacheClient != nil {
		if err := a.CacheClient.PingConnection(); err != nil {
			hcData.CacheClient = fmt.Sprintf("Failure: %s", err.Error())
			allFine = false
		} else {
			hcData.CacheClient = "OK"
		}
	}

	// Check whether all went fine or not, and return result
	if !allFine {
		return BuildResponse(hcData, "", http.StatusInternalServerError)
	}
	return BuildResponse(hcData, "", http.StatusOK)
}
