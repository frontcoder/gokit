package gobase

import (
	"encoding/json"
	"errors"
	"html/template"
	"log"
	"math/rand"
	"net/url"
	"reflect"
	"strings"
	"time"
)

// NowMillis - Util function that gets current time in milliseconds
func NowMillis() int64 {
	return time.Now().UnixNano() * int64(time.Nanosecond) / int64(time.Millisecond)
}

// CheckError - Util function that checks an error and logs/acts by severity level
func CheckError(err error, sev string) {
	if err != nil {
		switch sev {
		case "fatal":
			log.Fatal(err)
		case "panic":
			log.Panic(err)
		default:
			log.Println(err)
		}
	}
}

// FromJSON Fills JSON into model
func FromJSON(m interface{}, in []byte) error {
	return json.Unmarshal(in, m)
}

// ToJSON Exports the model into JSON
func ToJSON(m interface{}) ([]byte, error) {
	return json.Marshal(m)
}

// TransformStruct transforms a struct (in) into another (out - should be address)
func TransformStruct(in interface{}, out interface{}) error {
	inBytes, err := json.Marshal(in)
	if err != nil {
		return err
	}
	err = json.Unmarshal(inBytes, out)
	return err
}

// BuildResponse - Util function that builds a Response
func BuildResponse(data interface{}, err string, code int) *Response {
	var theError error
	if err != "" {
		theError = errors.New(err)
	}
	return &Response{
		Data:  data,
		Error: theError,
		Code:  code,
	}
}

// Global constants and variables
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var randSrc = rand.NewSource(time.Now().UnixNano())

// RandomString generates a random string
func RandomString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, randSrc.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = randSrc.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

// TemplateFunctionsMap Define some handy functions for using inside templates
var TemplateFunctionsMap = template.FuncMap{
	"ToLower": strings.ToLower,
	"ToUpper": strings.ToUpper,
}

// ExtractFieldNames - Util function that extracts the field names of a struct
func ExtractFieldNames(in interface{}) []string {
	var names []string
	value := reflect.Indirect(reflect.ValueOf(in))
	if !value.IsValid() {
		return names
	}
	valueType := value.Type()
	count := valueType.NumField()
	for i := 0; i < count; i++ {
		names = append(names, valueType.Field(i).Name)
	}
	return names
}

// GetFieldByName - Util function that gets a struct field by its name
func GetFieldByName(in interface{}, name string) interface{} {
	value := reflect.Indirect(reflect.ValueOf(in))
	if !value.IsValid() {
		return nil
	}
	field := value.FieldByName(name)
	if !field.IsValid() {
		return nil
	}
	return field.Interface()
}

// SetFieldByName - Util function that sets a struct field value by its name
func SetFieldByName(in interface{}, val interface{}, name string) error {
	value := reflect.Indirect(reflect.ValueOf(in))
	if !value.IsValid() {
		return errors.New("could not reflect input structure")
	}
	field := value.FieldByName(name)
	if !field.IsValid() {
		return errors.New("could not reflect input structure required field " + name)
	}
	if !field.CanSet() {
		return errors.New("cannot change value of input structure required field " + name)
	}
	refVal := reflect.ValueOf(val)
	if !refVal.IsValid() {
		return errors.New("could not reflect the value to set")
	}
	field.Set(refVal)
	return nil
}

// FromMap Fills a map into model
func FromMap(m interface{}, in map[string]interface{}) error {
	tmp, err := json.Marshal(in)
	if err != nil {
		return err
	}
	return json.Unmarshal(tmp, m)
}

// ToMap Exports the model into a map
func ToMap(m interface{}) (map[string]interface{}, error) {
	tmp, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	var res map[string]interface{}
	err = json.Unmarshal(tmp, &res)
	return res, err
}

// FieldToCustomTagMap exports a map from a struct field, given a named tag
func FieldToCustomTagMap(field reflect.StructField, tagName string) (map[string]interface{}, error) {

	// Get the field tag value
	tag := field.Tag.Get(tagName)

	// Skip if tag is not defined or ignored
	if tag == "" || tag == "-" {
		return nil, nil
	}

	// Split tag into comma-separated values
	args := strings.Split(tag, ",")

	// No args, no tags
	if len(args) < 1 {
		return nil, errors.New("No tag values in tag " + tag)
	}

	// Declare small map here for this field
	fMap := make(map[string]interface{})

	// Loop over fields
	for _, tf := range args {

		// Skip if empty
		if tf == "" {
			continue
		}

		// Get duple, fail if no complete
		tfEntries := strings.Split(tf, "=")
		if len(tfEntries) < 2 {
			log.Printf("Error parsing struct tag %s", tf)
			continue
		}

		// Get key, fail if empty
		tfKey := strings.TrimSpace(tfEntries[0])
		if tfKey == "" {
			continue
		}

		// Get value, fail if empty
		tfVal := strings.TrimSpace(tfEntries[1])
		if tfKey == "" {
			continue
		}

		// Add entry to map
		fMap[tfKey] = tfVal
	}

	// Return generated map
	return fMap, nil
}

// ToCustomTagMap exports a map from a struct given a named tag
func ToCustomTagMap(m interface{}, tagName string) (map[string]interface{}, error) {

	// Declare map
	res := make(map[string]interface{})

	// Fail if no value on interface
	if m == nil {
		return nil, errors.New("Passed interface in nil")
	}

	// Fail if no tag name
	if tagName == "" {
		return nil, errors.New("No tag name specified")
	}

	// ValueOf returns a Value representing the run-time data
	v := reflect.ValueOf(m)

	// Loop over fields
	for i := 0; i < v.NumField(); i++ {

		// Get the field
		field := v.Type().Field(i)

		// Call the per-field function
		tagsMap, err := FieldToCustomTagMap(field, tagName)
		if err != nil {
			return nil, err
		}

		// Add field map to global one, mapped to the field original key always!!
		res[field.Name] = tagsMap
	}

	// Return final result map
	return res, nil
}

// ExtractMapBool extracts a bool from a map
// Important! We assume string keyed maps here, as it covers practically 100% of our map keys
// Second returned parameter is a boolean flag stating if property was found
// Third returned parameter is a boolean flag stating if property was found and successfully parsed
func ExtractMapBool(in map[string]interface{}, p string) (bool, bool, bool) {
	property := in[p]
	if property == nil {
		return false, false, false
	}
	parsedProperty, ok := property.(bool)
	return parsedProperty, true, ok
}

// ExtractMapInt extracts an int from a map
// Important! We assume string keyed maps here, as it covers practically 100% of our map keys
// Second returned parameter is a boolean flag stating if property was found
// Third returned parameter is a boolean flag stating if property was found and successfully parsed
func ExtractMapInt(in map[string]interface{}, p string) (int, bool, bool) {
	property := in[p]
	if property == nil {
		return 0, false, false
	}
	parsedProperty, ok := property.(int)
	return parsedProperty, true, ok
}

// ExtractMapString extracts a string from a map
// Important! We assume string keyed maps here, as it covers practically 100% of our map keys
// Second returned parameter is a boolean flag stating if property was found
// Third returned parameter is a boolean flag stating if property was found and successfully parsed
func ExtractMapString(in map[string]interface{}, p string) (string, bool, bool) {
	property := in[p]
	if property == nil {
		return "", false, false
	}
	parsedProperty, ok := property.(string)
	return parsedProperty, true, ok
}

// URLValuesToMap converts to underlying type or single valued map (second parameter)
// WTF: for some reson it's not possible to directly cast this?
func URLValuesToMap(in url.Values, single bool) map[string]interface{} {
	if in == nil {
		return nil
	}
	outMap := make(map[string]interface{})
	for k, v := range in {
		if single {
			outMap[k] = v[0]
		} else {
			outMap[k] = v
		}
	}
	return outMap
}

// MergeMaps merges an arbitrary number of maps into one
func MergeMaps(in ...map[string]interface{}) map[string]interface{} {
	if in == nil {
		return nil
	}
	outMap := make(map[string]interface{})
	for _, m := range in {
		for k, v := range m {
			outMap[k] = v
		}
	}
	return outMap
}
